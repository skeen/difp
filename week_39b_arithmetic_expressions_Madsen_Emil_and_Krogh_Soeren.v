(* week_39b_arithmetic_expressions.v *)
(* dIFP 2014-2015, Q1, Week 38 *)
(* Olivier Danvy <danvy@cs.au.dk> *)

(* Handin by;
 * Emil 'Skeen' Madsen - 20105376
 * Søren Krogh         - 20105661
 *)
(* Sorry for the late handin! *)

(* TODO: Remove all the remaining TODO's in the file, by DOing *)
(* TODO: Distringuish between 'byte_code_instruction list' and 'byte_code_program' on a logical level *)

(* ********** *)

Require Import Arith Bool List unfold_tactic.

(* ********** *)
(* Source syntax: *)

Inductive arithmetic_expression : Type :=
  | Lit : nat -> arithmetic_expression
  | Plus : arithmetic_expression -> arithmetic_expression -> arithmetic_expression
  | Times : arithmetic_expression -> arithmetic_expression -> arithmetic_expression.

(* Exercise 0:
   Write samples of arithmetic expressions.
*)
Definition ame_0 :=
  Lit 1.

Definition ame_1 :=
  (Plus (Lit 1) (Lit 2)).

Definition ame_2 :=
  (Times (Plus (Lit 1) (Lit 2)) (Lit 3)).

(* Exercise 1:
   Write unit tests.
*)
Notation "A =n= B" := (beq_nat A B) (at level 70, right associativity).

Definition unit_test_for_arithmetics (candidate : arithmetic_expression -> nat) :=
  (candidate ame_0 =n= 1)
  &&
  (candidate ame_1 =n= 3)
  &&
  (candidate ame_2 =n= 9)
  &&
  (candidate (Plus ame_1 ame_2) =n= 12)
  .

(* ********** *)

Definition specification_of_interpret (interpret : arithmetic_expression -> nat) :=
  (forall n : nat,
     interpret (Lit n) = n)
  /\
  (forall ae1 ae2 : arithmetic_expression,
     interpret (Plus ae1 ae2) = (interpret ae1) + (interpret ae2))
  /\
  (forall ae1 ae2 : arithmetic_expression,
     interpret (Times ae1 ae2) = (interpret ae1) * (interpret ae2)).

(* Confirm that unit tests adhere to the specification *)
Lemma unit_tests_are_passed_by_specification_of_interpret :
  forall (interpreter : arithmetic_expression -> nat),
    specification_of_interpret interpreter ->
    unit_test_for_arithmetics interpreter = true.
Proof.
  intro interpreter.
  intro s_interpret.
  unfold specification_of_interpret in s_interpret.
  destruct s_interpret as [intr_lit [intr_plus intr_times]].
  (* unfold everything to numbers *)
  unfold unit_test_for_arithmetics.
  unfold ame_0.
  unfold ame_1.
  unfold ame_2.
  rewrite ->3 (intr_plus).
  rewrite -> (intr_times).
  rewrite -> (intr_plus).
  rewrite ->3 (intr_lit).
  (* Magic via normal form *)
  ring.
Qed.

Lemma there_is_only_one_interpret :
  forall (intr1 intr2 : arithmetic_expression -> nat),
    specification_of_interpret intr1 ->
    specification_of_interpret intr2 ->
    forall ae,
      intr1 ae = intr2 ae.
Proof.
  intros intr1 intr2.
  intros s_intr1 s_intr2.
  intro ae.
  unfold specification_of_interpret in s_intr1.
  destruct s_intr1 as [intr1_lit [intr1_plus intr1_times]].
  unfold specification_of_interpret in s_intr2.
  destruct s_intr2 as [intr2_lit [intr2_plus intr2_times]].
  induction ae as [n | ae1 IHae1 ae2 IHae2 | ae1 IHae1 ae2 IHae2].
    rewrite -> (intr1_lit n).
    rewrite -> (intr2_lit n).
    reflexivity.

    rewrite -> (intr1_plus ae1 ae2).
    rewrite -> (IHae1).
    rewrite -> (intr2_plus ae1 ae2).
    rewrite -> (IHae2).
    reflexivity.

    rewrite -> (intr1_times ae1 ae2).
    rewrite -> (IHae1).
    rewrite -> (intr2_times ae1 ae2).
    rewrite -> (IHae2).
    reflexivity.
Qed.

(* Exercise 2:
   Define an interpreter as a function
   that satisfies the specification above
   and verify that it passes the unit tests.
*)
Fixpoint interpreter (ae : arithmetic_expression) : nat :=
  match ae with
    | Lit n =>
      n
    | Plus ae1 ae2 =>
      (interpreter ae1) + (interpreter ae2)
    | Times ae1 ae2 =>
      (interpreter ae1) * (interpreter ae2)
  end.

(* Check against unit tests *)
Compute unit_test_for_arithmetics interpreter.

Lemma interpreter_lit :
  forall n : nat,
    interpreter (Lit n) = n.
Proof.
  unfold_tactic interpreter.
Qed.

Lemma interpreter_plus :
  forall ae1 ae2 : arithmetic_expression,
    interpreter (Plus ae1 ae2) = (interpreter ae1) + (interpreter ae2).
Proof.
  unfold_tactic interpreter.
Qed.

Lemma interpreter_times :
  forall ae1 ae2 : arithmetic_expression,
    interpreter (Times ae1 ae2) = (interpreter ae1) * (interpreter ae2).
Proof.
  unfold_tactic interpreter.
Qed.

Lemma interpreter_holds_specification_of_interpret :
  specification_of_interpret interpreter.
Proof.
  unfold specification_of_interpret.
  split.
    intro n.
    apply (interpreter_lit n).

    split.
      intros ae1 ae2.
      apply (interpreter_plus ae1 ae2).

      intros ae1 ae2.
      apply (interpreter_times ae1 ae2).
Qed.

(* Byte-code instructions: *)

Inductive byte_code_instruction : Type :=
  | PUSH : nat -> byte_code_instruction
  | ADD : byte_code_instruction
  | MUL : byte_code_instruction.

(* ********** *)

(* Byte-code programs: *)

Definition byte_code_program := list byte_code_instruction.

(* Data stack: *)

Definition data_stack := list nat.

(* ********** *)

(* Exercise 3:
   specify a function
     execute_byte_code_instruction : instr -> data_stack -> data_stack
   that executes a byte-code instruction, given a data stack
   and returns this stack after the instruction is executed.

   * Executing (PUSH n) given s has the effect of pushing n on s.

   * Executing ADD given s has the effect of popping two numbers
     from s and then pushing the result of adding them.

   * Executing MUL given s has the effect of popping two numbers
     from s and then pushing the result of multiplying them.

   For now, if the stack underflows, just assume it contains zeroes.
*)
(* List equality needed for unit tests *)
Fixpoint equal_list_nat (xs ys : list nat) :=
  match xs with
    | nil =>
      match ys with
        | nil => true
        | y :: ys' => false
      end
    | x :: xs' =>
      match ys with
        | nil => false
        | y :: ys' => (beq_nat x y) && (equal_list_nat xs' ys')
      end
  end.

Notation "A =l= B" := (equal_list_nat A B) (at level 70, right associativity).

(* Valid unit tests *)
Definition unit_test_for_virtual_machine_step (candidate : byte_code_instruction -> data_stack -> data_stack) :=
  (candidate (PUSH 0) nil =l= (0 :: nil))
  &&
  (candidate (PUSH 1) nil =l= (1 :: nil))
  &&
  (candidate (PUSH 2) nil =l= (2 :: nil))
  &&
  (candidate (PUSH 0) (1 :: nil) =l= (0 :: 1 :: nil))
  &&
  (candidate (PUSH 1) (2 :: 3 :: nil) =l= (1 :: 2 :: 3 :: nil))
  && 
  (candidate (ADD) (0 :: 0 :: nil) =l= (0 :: nil))
  && 
  (candidate (ADD) (1 :: 0 :: nil) =l= (1 :: nil))
  && 
  (candidate (ADD) (0 :: 1 :: nil) =l= (1 :: nil))
  &&
  (candidate (ADD) (1 :: 1 :: nil) =l= (2 :: nil))
  &&
  (candidate (ADD) (5 :: 3 :: nil) =l= (8 :: nil))
  &&
  (candidate (MUL) (0 :: 1 :: nil) =l= (0 :: nil))
  &&
  (candidate (MUL) (1 :: 0 :: nil) =l= (0 :: nil))
  &&
  (candidate (MUL) (5 :: 0 :: nil) =l= (0 :: nil))
  &&
  (candidate (MUL) (1 :: 1 :: nil) =l= (1 :: nil))
  &&
  (candidate (MUL) (1 :: 2 :: nil) =l= (2 :: nil))
  &&
  (candidate (MUL) (1 :: 5 :: nil) =l= (5 :: nil))
  &&
  (candidate (MUL) (3 :: 5 :: nil) =l= (15 :: nil))
  .

(* Invalid unit tests *)
Definition unit_test_for_virtual_machine_step_invalid (candidate : byte_code_instruction -> data_stack -> data_stack) :=
  (candidate (ADD) nil =l= (0 :: nil))
  &&
  (candidate (ADD) (0 :: nil) =l= (0 :: nil))
  && 
  (candidate (ADD) (1 :: nil) =l= (1 :: nil))
  && 
  (candidate (ADD) (5 :: nil) =l= (5 :: nil))
  &&
  (candidate (MUL) nil =l= (0 :: nil))
  &&
  (candidate (MUL) (0 :: nil) =l= (0 :: nil))
  &&
  (candidate (MUL) (1 :: nil) =l= (0 :: nil))
  &&
  (candidate (MUL) (5 :: nil) =l= (0 :: nil))
  .

(* Virtual machine, which pads with zeros *)
Definition specification_of_virtual_machine_step
   (virtual_machine : byte_code_instruction -> data_stack -> data_stack) :=
  (forall (n : nat)
          (ds : data_stack),
     virtual_machine (PUSH n) ds = n :: ds)
  /\
  (virtual_machine ADD (nil) = (0 :: nil))
  /\
  (forall (a : nat),
     virtual_machine ADD (a::nil) = ((a+0) :: nil))
  /\
  (forall (a b : nat)
          (ds : data_stack),
     virtual_machine ADD (a::b::ds) = ((b+a) :: ds))
  /\
  (virtual_machine MUL (nil) = (0 :: nil))
  /\
  (forall (a : nat),
     virtual_machine MUL (a::nil) = ((a*0) :: nil))
  /\
  (forall (a b : nat)
          (ds : data_stack),
     virtual_machine MUL (a::b::ds) = ((b*a) :: ds)).

(* Confirm that unit tests adhere to the specification *)
Lemma unit_tests_are_passed_by_specification_of_virtual_machine_step :
  forall (virtual_machine : byte_code_instruction -> data_stack -> data_stack),
    specification_of_virtual_machine_step virtual_machine ->
    (unit_test_for_virtual_machine_step virtual_machine)
      &&
    (unit_test_for_virtual_machine_step_invalid virtual_machine) = true.
Proof.
  intro vm.
  intro s_vm.
  unfold specification_of_virtual_machine_step in s_vm.
  destruct s_vm as [vm_push [vm_add_0 [vm_add_1 [vm_add_2 [vm_mul_0 [vm_mul_1 vm_mul_2]]]]]].
  (* unfold everything to numbers *)
  unfold unit_test_for_virtual_machine_step.
  unfold unit_test_for_virtual_machine_step_invalid.
  rewrite ->5 vm_push.
  rewrite ->5 vm_add_2.
  rewrite ->7 vm_mul_2.
  rewrite ->3 vm_add_1.
  rewrite ->3 vm_mul_1.
  rewrite -> vm_add_0.
  rewrite -> vm_mul_0.
  (* Magic via normal form *)
  ring.
Qed.

Lemma there_is_only_one_virtual_machine_step :
  forall (vm1 vm2 : byte_code_instruction -> data_stack -> data_stack),
   specification_of_virtual_machine_step vm1 ->
   specification_of_virtual_machine_step vm2 ->
   forall (instruction : byte_code_instruction)
          (ds : data_stack),
     vm1 instruction ds = vm2 instruction ds.
Proof.
  intros vm1 vm2.
  intros s_vm1 s_vm2.
  intro instruction.
  intro ds.
  unfold specification_of_virtual_machine_step in s_vm1.
  destruct s_vm1 as [vm1_push [vm1_add_0 [vm1_add_1 [vm1_add_2 [vm1_mul_0 [vm1_mul_1 vm1_mul_2]]]]]].
  unfold specification_of_virtual_machine_step in s_vm2.
  destruct s_vm2 as [vm2_push [vm2_add_0 [vm2_add_1 [vm2_add_2 [vm2_mul_0 [vm2_mul_1 vm2_mul_2]]]]]].
  induction instruction as [n | | ].
    rewrite -> (vm1_push n ds).
    rewrite -> (vm2_push n ds).
    reflexivity.

    case ds.
      rewrite -> (vm1_add_0).
      rewrite -> (vm2_add_0).
      reflexivity.

      intro a.
      intro rest.
      case rest.
        rewrite -> (vm1_add_1 a).
        rewrite -> (vm2_add_1 a).
        reflexivity.

        intro b.
        intro rest'.
        rewrite -> (vm1_add_2 a b rest').
        rewrite -> (vm2_add_2 a b rest').
        reflexivity.

    case ds.
      rewrite -> (vm1_mul_0).
      rewrite -> (vm2_mul_0).
      reflexivity.

      intro a.
      intro rest.
      case rest.
        rewrite -> (vm1_mul_1 a).
        rewrite -> (vm2_mul_1 a).
        reflexivity.

        intro b.
        intro rest'.
        rewrite -> (vm1_mul_2 a b rest').
        rewrite -> (vm2_mul_2 a b rest').
        reflexivity.
Qed.

(* Implementation, which pads with zeroes *)
Fixpoint execute_byte_code_instruction
  (instr : byte_code_instruction)
  (ds : data_stack) : data_stack :=
  match instr with
    | PUSH n =>
      (n :: ds)
    | ADD =>
      match ds with
        | (nil) =>
          ((0+0) :: nil)
        | (a :: nil) =>
          ((a+0) :: nil)
        | (a :: b :: ds') =>
          ((b+a) :: ds')
      end
    | MUL =>
      match ds with
        | (nil) =>
          ((0*0) :: nil)
        | (a :: nil) =>
          ((a*0) :: nil)
        | (a :: b :: ds') =>
          ((b*a) :: ds')
      end
end.

Compute unit_test_for_virtual_machine_step execute_byte_code_instruction.
Compute unit_test_for_virtual_machine_step_invalid execute_byte_code_instruction.

Lemma execute_push :
  forall (ds : data_stack)
         (n : nat),
    execute_byte_code_instruction (PUSH n) ds = (n :: ds).
Proof.
  unfold_tactic execute_byte_code_instruction.
Qed.

Lemma execute_plus_nil :
  execute_byte_code_instruction ADD nil = (0 :: nil).
Proof.
  unfold_tactic execute_byte_code_instruction.
Qed.

Lemma execute_plus_a_nil :
  forall (a : nat),
    execute_byte_code_instruction ADD (a :: nil) = ((a+0) :: nil).
Proof.
  unfold_tactic execute_byte_code_instruction.
Qed.

Lemma execute_plus_a_b :
  forall (ds : data_stack)
         (a b : nat),
    execute_byte_code_instruction ADD (a :: b :: ds) = ((b+a) :: ds).
Proof.
  unfold_tactic execute_byte_code_instruction.
Qed.

Lemma execute_times_nil :
  execute_byte_code_instruction MUL nil = (0 :: nil).
Proof.
  unfold_tactic execute_byte_code_instruction.
Qed.

Lemma execute_times_a_nil :
  forall (a : nat),
    execute_byte_code_instruction MUL (a :: nil) = ((a*0) :: nil).
Proof.
  unfold_tactic execute_byte_code_instruction.
Qed.

Lemma execute_times_a_b :
  forall (ds : data_stack)
         (a b : nat),
    execute_byte_code_instruction MUL (a :: b :: ds) = ((b*a) :: ds).
Proof.
  unfold_tactic execute_byte_code_instruction.
Qed.

Lemma execute_instruction_holds_specification_of_virtual_machine_step :
  specification_of_virtual_machine_step execute_byte_code_instruction.
Proof.
  unfold specification_of_virtual_machine_step.
  split.
    intro n.
    intro ds.
    apply (execute_push ds n).

    split.
      apply (execute_plus_nil).

      split.
        intro a.
        apply (execute_plus_a_nil a).

        split.
          intros a b.
          intro ds.
          apply (execute_plus_a_b ds a b).

    split.
      apply (execute_times_nil).
      
      split.
        intro a.
        apply (execute_times_a_nil a).
        
        intros a b.
        intro ds.
        apply (execute_times_a_b ds a b).
Qed.

(* ********** *)

(* Exercise 4:
   Define a function
     execute_byte_code_program : byte_code_program -> data_stack -> data_stack
   that executes a given byte-code program on a given data stack,
   and returns this stack after the program is executed.
*)
Definition specification_of_virtual_machine (virtual_machine : byte_code_program -> data_stack -> data_stack) :=
  (forall (ds : data_stack),
     virtual_machine nil ds = ds)
  /\
  (forall (virtual_machine_step : byte_code_instruction -> data_stack -> data_stack),
     specification_of_virtual_machine_step virtual_machine_step ->
      forall (instruction : byte_code_instruction) 
             (program' : byte_code_program)
             (ds : data_stack),
     virtual_machine (instruction::program') ds = virtual_machine program' (virtual_machine_step instruction ds))
.

Fixpoint execute_byte_code_program
         (program : byte_code_program)
         (ds : data_stack) : data_stack :=
  match program with
    | (nil) =>
        ds
    | (instr :: program') =>
        execute_byte_code_program program' (execute_byte_code_instruction instr ds)
  end.
(* Due to simplicity in implementation of the above,
 * the following has tasks been left out:
 * - Unit tests
 * - Proof that unit tests adhere to specification
 * - Proof of uniqueness
 * TODO: These are left as an exercise to the corrector.
 *)

(* ********** *)

(* Exercise 5:
   Prove that for all programs p1, p2 and data stacks s,
   executing (p1 ++ p2) with s
   gives the same result as
   (1) executing p1 with s, and then
   (2) executing p2 with the resulting stack.
*)
Lemma unfold_append_bc :
  forall (bcis : list byte_code_instruction),
    nil ++ bcis = bcis.
Proof.
  apply app_nil_l.
Qed.

Lemma unfold_append_ic :
  forall (bci1 : byte_code_instruction) (bci1s' bci2s : list byte_code_instruction),
    (bci1 :: bci1s') ++ bci2s = bci1 :: (bci1s' ++ bci2s).
Proof.
  intros bci1 bci1s' bci2s.
  symmetry.
  apply app_comm_cons.
Qed.

Lemma execute_program_nil_does_nothing :
  forall (s : data_stack),
  execute_byte_code_program nil s = s.
Proof.
  unfold_tactic execute_byte_code_program.
Qed.

Lemma execute_program_ic :
  forall (instr : byte_code_instruction)
         (p : byte_code_program)
         (s : data_stack),
    execute_byte_code_program (instr :: p) s = execute_byte_code_program p (execute_byte_code_instruction instr s).
Proof.
  unfold_tactic execute_byte_code_program.
Qed.

Lemma execute_instruction_is_as_good_as_any :
  forall (vm_step : byte_code_instruction -> data_stack -> data_stack),
    specification_of_virtual_machine_step vm_step ->
    forall (instruction : byte_code_instruction)
           (ds : data_stack),
    vm_step instruction ds = execute_byte_code_instruction instruction ds.
Proof.
  intro vm_step.
  intro s_vm_step.
  intro instruction.
  intro ds.
  rewrite -> (there_is_only_one_virtual_machine_step vm_step execute_byte_code_instruction).
  reflexivity.

  apply (s_vm_step).
  apply (execute_instruction_holds_specification_of_virtual_machine_step).
Qed.

Lemma execute_holds_specification_of_virtual_machine :
  specification_of_virtual_machine execute_byte_code_program.
Proof.
  unfold specification_of_virtual_machine.
  split.
    intro ds.
    apply (execute_program_nil_does_nothing ds).

    intro vm_step.
    intro s_vm_step.
    intro instr.
    intro program'.
    intro ds.
    rewrite (execute_program_ic instr program' ds).
    rewrite -> (execute_instruction_is_as_good_as_any vm_step s_vm_step instr ds).
    reflexivity.
Qed.

Lemma executing_p1p2_equals_executing_p1_then_p2 :
  forall (virtual_machine : byte_code_program -> data_stack -> data_stack),
    specification_of_virtual_machine virtual_machine ->
    forall (virtual_machine_step : byte_code_instruction -> data_stack -> data_stack),
      specification_of_virtual_machine_step virtual_machine_step ->
  forall (p1 p2 : byte_code_program)
         (ds : data_stack),
    virtual_machine (p1 ++ p2) ds = virtual_machine p2 (virtual_machine p1 ds).
Proof.
  intro vm.
  intro s_vm.
  intro vm_step.
  intro s_vm_step.
  unfold specification_of_virtual_machine in s_vm.
  destruct s_vm as [vm_bc vm_ic].
  intros p1 p2.
  induction p1 as [ | instr program' IHp'].
    intro ds.
    Check unfold_append_bc.
    rewrite -> (unfold_append_bc p2).
    rewrite -> (vm_bc ds).
    reflexivity.

    intro ds.
    Check unfold_append_ic.
    rewrite -> (unfold_append_ic instr program' p2).
    rewrite -> (vm_ic vm_step s_vm_step instr (program' ++ p2) ds).
    rewrite -> (IHp' (vm_step instr ds)).
    rewrite <- (vm_ic vm_step s_vm_step instr program' ds).
    reflexivity.
Qed.

(* TODO: Unit tests for compile *)

Definition specification_of_compile (compile : arithmetic_expression -> byte_code_program) :=
  (forall n : nat,
     compile (Lit n) = PUSH n :: nil)
  /\
  (forall ae1 ae2 : arithmetic_expression,
     compile (Plus ae1 ae2)  = (compile ae1) ++ (compile ae2) ++ (ADD :: nil))
  /\
  (forall ae1 ae2 : arithmetic_expression,
     compile (Times ae1 ae2) = (compile ae1) ++ (compile ae2) ++ (MUL :: nil)).

(* TODO: Check that unit tests adhere to specification *)

(* Exercise 6:
   Define a compiler as a function
   that satisfies the specification above
   and uses list concatenation, i.e., ++.
*)
Fixpoint compiler
         (ame : arithmetic_expression) : byte_code_program :=
  match ame with
    | Lit n =>
      (PUSH n :: nil)
    | Plus ae1 ae2 =>
      (compiler ae1) ++ (compiler ae2) ++ (ADD :: nil)
    | Times ae1 ae2 =>
      (compiler ae1) ++ (compiler ae2) ++ (MUL :: nil)
  end.

(* TODO: Check compiler against unit tests *)

Lemma compiler_lit :
  forall n : nat,
    compiler (Lit n) = (PUSH n :: nil).
Proof.
  unfold_tactic compiler.
Qed.

Lemma compiler_plus :
  forall (ae1 ae2 : arithmetic_expression),
    compiler (Plus ae1 ae2) = (compiler ae1) ++ (compiler ae2) ++ (ADD :: nil).
Proof.
  unfold_tactic compiler.
Qed.

Lemma compiler_times :
  forall (ae1 ae2 : arithmetic_expression),
    compiler (Times ae1 ae2) = (compiler ae1) ++ (compiler ae2) ++ (MUL :: nil).
Proof.
  unfold_tactic compiler.
Qed.

Lemma compiler_satisfy_compile :
  specification_of_compile compiler.
Proof.
  unfold specification_of_compile.
  split.
    intro n.
    apply (compiler_lit n).

    split.
      intros ae1 ae2.
      apply (compiler_plus ae1 ae2).

      intros ae1 ae2.
      apply (compiler_times ae1 ae2).
Qed.
    
(* Exercise 7:
   Write a compiler as a function with an accumulator
   that does not use ++ but :: instead,
   and prove it equivalent to the compiler of Exercise 6.
*)
Fixpoint compiler_acc
         (ame : arithmetic_expression)
         (acc : byte_code_program) : byte_code_program :=
  match ame with
    | Lit n =>
      (PUSH n :: acc)
    | Plus ae1 ae2 =>
      (compiler_acc ae1 (compiler_acc ae2 (ADD :: acc)))
    | Times ae1 ae2 =>
      (compiler_acc ae1 (compiler_acc ae2 (MUL :: acc)))
  end.

(* TODO: Check compiler against unit tests *)

Lemma compiler_acc_lit :
  forall (n : nat)
         (acc : byte_code_program),
    compiler_acc (Lit n) acc = (PUSH n :: acc).
Proof.
  unfold_tactic compiler_acc.
Qed.

Lemma compiler_acc_plus :
  forall (ae1 ae2 : arithmetic_expression)
         (acc : byte_code_program),
    compiler_acc (Plus ae1 ae2) acc = (compiler_acc ae1 (compiler_acc ae2 (ADD :: acc))).
Proof.
  unfold_tactic compiler_acc.
Qed.

Lemma compiler_acc_times :
  forall (ae1 ae2 : arithmetic_expression)
         (acc : byte_code_program),
    compiler_acc (Times ae1 ae2) acc = (compiler_acc ae1 (compiler_acc ae2 (MUL :: acc))).
Proof.
  unfold_tactic compiler_acc.
Qed.

Fixpoint compiler_acc_entry
         (ame : arithmetic_expression) : byte_code_program :=
  compiler_acc ame nil.

(* Wierd trick, as unfold did; ((fix : whatever) application) *)
Lemma compiler_entry_to_compiler :
  forall ae1 : arithmetic_expression,
    compiler_acc_entry ae1 = compiler_acc ae1 nil.
Proof.
  induction ae1.
    unfold compiler_acc_entry.
    reflexivity.

    unfold compiler_acc_entry.
    reflexivity.

    unfold compiler_acc_entry.
    reflexivity.
Qed.

(* TODO: Rewrite to using two byte_code_instruction lists rather than nil and one *)
Lemma appending_unused_environment_is_okay :
  forall (ae1  : arithmetic_expression)
         (ae2 : byte_code_program),
    compiler_acc ae1 nil ++ ae2 = compiler_acc ae1 (nil ++ ae2).
Proof.
  intros ae1 ae2.
  rewrite -> (unfold_append_bc ae2).
  revert ae2.
  induction ae1.
    intro ae2.
    rewrite ->2 (compiler_acc_lit).
    Check unfold_append_ic.
    rewrite -> (unfold_append_ic (PUSH n) nil ae2).
    rewrite -> (unfold_append_bc ae2).
    reflexivity.

    intro ae2.
    rewrite ->2 (compiler_acc_plus).
    rewrite <- (IHae1_2 (ADD :: ae2)).
    rewrite <- (IHae1_1 (compiler_acc ae1_2 nil ++ ADD :: ae2)).
    rewrite <- (IHae1_2 (ADD :: nil)).
    rewrite <- (IHae1_1 (compiler_acc ae1_2 nil ++ ADD :: nil)).
    rewrite <- (unfold_append_bc ae2) at 2.
    rewrite ->2 app_ass.
    Check unfold_append_ic.
    rewrite -> (unfold_append_ic ADD nil ae2).
    reflexivity.

    intro ae2.
    rewrite ->2 (compiler_acc_times).
    rewrite <- (IHae1_2 (MUL :: ae2)).
    rewrite <- (IHae1_1 (compiler_acc ae1_2 nil ++ MUL :: ae2)).
    rewrite <- (IHae1_2 (MUL :: nil)).
    rewrite <- (IHae1_1 (compiler_acc ae1_2 nil ++ MUL :: nil)).
    rewrite <- (unfold_append_bc ae2) at 2.
    rewrite ->2 app_ass.
    Check unfold_append_ic.
    rewrite -> (unfold_append_ic MUL nil ae2).
    reflexivity.
Qed.

Lemma appending_environment_is_okay_entry :
  forall (ae1 : arithmetic_expression)
         (ae2 : byte_code_program),
    compiler_acc ae1 nil ++ ae2 = compiler_acc ae1 ae2.
Proof.
  intro ae1.
  intro ae2.
  Check appending_unused_environment_is_okay.
  rewrite -> (appending_unused_environment_is_okay ae1 ae2).
  Check unfold_append_bc.
  rewrite -> (unfold_append_bc ae2).
  reflexivity.
Qed.

Lemma eureka_folding :
  forall (instruction : byte_code_instruction)
         (ae1 ae2 : arithmetic_expression),
 compiler_acc ae1 (compiler_acc ae2 (instruction :: nil)) =
   compiler_acc ae1 nil ++ compiler_acc ae2 nil ++ instruction :: nil.
Proof.
  intro instruction.
  intros ae1 ae2.
  (* No arguments in order to allow 2 applications *)
  rewrite ->2 appending_environment_is_okay_entry.
  reflexivity.
Qed.

Lemma compiler_acc_satisfy_compile :
  specification_of_compile compiler_acc_entry.
Proof.
  unfold specification_of_compile.
  split.
    intro n.
    apply (compiler_acc_lit n nil).

    split.
      intros ae1 ae2.
      rewrite ->3 compiler_entry_to_compiler.
      rewrite -> (compiler_acc_plus ae1 ae2 nil).
      rewrite -> (eureka_folding ADD ae1 ae2).
      reflexivity.

      intros ae1 ae2.
      rewrite ->3 compiler_entry_to_compiler.
      rewrite -> (compiler_acc_times ae1 ae2 nil).
      rewrite -> (eureka_folding MUL ae1 ae2).
      reflexivity.
Qed.

Proposition there_is_only_one_compile :
  forall (co1 co2 : arithmetic_expression -> byte_code_program),
     specification_of_compile co1 ->
     specification_of_compile co2 ->
     forall ame : arithmetic_expression,
       co1 ame = co2 ame.
Proof.
  intros co1 co2.
  intros s_co1 s_co2.
  intro ame.
  unfold specification_of_compile in s_co1.
  destruct s_co1 as [co1_lit [co1_plus co1_times]].
  unfold specification_of_compile in s_co2.
  destruct s_co2 as [co2_lit [co2_plus co2_times]].
  induction ame as [lit | plus IHplus ame IHame | times IHtimes ame IHame].
    rewrite -> (co1_lit lit).
    rewrite -> (co2_lit lit).
    reflexivity.

    rewrite co1_plus.
    rewrite co2_plus.
    rewrite IHplus.
    rewrite IHame.
    reflexivity.

    rewrite co1_times.
    rewrite co2_times.
    rewrite IHtimes.
    rewrite IHame.
    reflexivity.
Qed.

Lemma compiler_and_compiler_acc_are_eq :
  forall ame : arithmetic_expression,
    compiler ame = compiler_acc_entry ame.
Proof.
  intro ame.
  Check there_is_only_one_compile.
  rewrite (there_is_only_one_compile compiler compiler_acc_entry).
  reflexivity.

  apply compiler_satisfy_compile.
  apply compiler_acc_satisfy_compile.
Qed.

(* ********** *)

(* Exercise 8:
   Prove that interpreting an arithmetic expression gives the same result
   as first compiling it and then executing the compiled program
   over an empty data stack.
*)
Lemma interpreting_equals_compile_and_execute :
 forall (interpreter : arithmetic_expression -> nat)
        (compiler : arithmetic_expression -> byte_code_program)
        (virtual_machine : byte_code_program -> data_stack -> data_stack)
        (virtual_machine_step : byte_code_instruction -> data_stack -> data_stack),
    specification_of_interpret interpreter ->
    specification_of_compile compiler ->
    specification_of_virtual_machine virtual_machine ->
    specification_of_virtual_machine_step virtual_machine_step ->
  forall (ae : arithmetic_expression) (ds : data_stack),
    virtual_machine (compiler ae) ds  = (interpreter ae ) :: ds.
Proof.
  intros interpreter compiler vm vm_step.
  intros s_interpret s_compile s_vm s_vm_step.
  intro ae.

  unfold specification_of_interpret in s_interpret.
  destruct s_interpret as [inter_lit [inter_plus inter_times]].
  
  unfold specification_of_compile in s_compile.
  destruct s_compile as [com_lit [com_plus com_times]].

  assert (s_vm' := s_vm).
  unfold specification_of_virtual_machine in s_vm'.
  destruct s_vm' as [vm_bc vm_ic].

  induction ae as [n | ae1 IHae1 ae2 IHae2 | ae1 IHae1 ae2 IHae2].
    intro ds.
    rewrite -> (inter_lit n).
    rewrite -> (com_lit n).
    rewrite -> (vm_ic vm_step s_vm_step (PUSH n) nil ds).
    rewrite -> (vm_bc (vm_step (PUSH n) ds)).
    unfold specification_of_virtual_machine_step in s_vm_step.
    destruct s_vm_step as [vm_step_push _].
    rewrite -> (vm_step_push n ds).
    reflexivity.

    intro ds.
    rewrite -> (inter_plus ae1 ae2).
    rewrite -> (com_plus ae1 ae2).
    Check executing_p1p2_equals_executing_p1_then_p2.
    rewrite ->2 (executing_p1p2_equals_executing_p1_then_p2 vm s_vm vm_step s_vm_step).
    rewrite -> (vm_ic vm_step s_vm_step ADD nil  (vm (compiler ae2) (vm (compiler ae1) ds))).
    rewrite -> (vm_bc (vm_step ADD (vm (compiler ae2) (vm (compiler ae1) ds)))).
    rewrite -> (IHae1 ds).
    rewrite -> (IHae2 (interpreter ae1 :: ds)).
    unfold specification_of_virtual_machine_step in s_vm_step.
    destruct s_vm_step as [_ [_ [_ [vm_step_plus_ab _]]]].
    rewrite -> (vm_step_plus_ab (interpreter ae2) (interpreter ae1) ds).
    reflexivity.

    intro ds.
    rewrite -> (inter_times ae1 ae2).
    rewrite -> (com_times ae1 ae2).
    Check executing_p1p2_equals_executing_p1_then_p2.
    rewrite ->2 (executing_p1p2_equals_executing_p1_then_p2 vm s_vm vm_step s_vm_step).
    rewrite -> (vm_ic vm_step s_vm_step MUL nil  (vm (compiler ae2) (vm (compiler ae1) ds))).
    rewrite -> (vm_bc (vm_step MUL (vm (compiler ae2) (vm (compiler ae1) ds)))).
    rewrite -> (IHae1 ds).
    rewrite -> (IHae2 (interpreter ae1 :: ds)).
    unfold specification_of_virtual_machine_step in s_vm_step.
    destruct s_vm_step as [_ [_ [_ [_ [_ [_  vm_step_mul_ab]]]]]].
    rewrite -> (vm_step_mul_ab (interpreter ae2) (interpreter ae1) ds).
    reflexivity.
Qed.

Fixpoint zero_or_top (stack : data_stack) : nat :=
  match stack with
    | nil => 0
    | a :: b => a
  end.

Lemma there_is_something_on_stack :
  forall (a : nat)
         (b : list nat),
  zero_or_top (a :: b) = a.
Proof.
  unfold_tactic zero_or_top.
Qed.

Definition run (program : byte_code_program) (virtual_machine : byte_code_program -> data_stack -> data_stack) : nat :=
  (zero_or_top (virtual_machine program nil)).

Lemma interpreting_equals_compile_and_run :
  forall (interpreter : arithmetic_expression -> nat)
         (compiler : arithmetic_expression -> byte_code_program)
         (virtual_machine : byte_code_program -> data_stack -> data_stack)
         (virtual_machine_step : byte_code_instruction -> data_stack -> data_stack),
    specification_of_interpret interpreter ->
    specification_of_compile compiler ->
    specification_of_virtual_machine virtual_machine ->
    specification_of_virtual_machine_step virtual_machine_step ->
    forall ame : arithmetic_expression,
      interpreter ame = run (compiler ame) virtual_machine.
Proof.
  intros interpreter compiler virtual_machine virtual_machine_step.
  intros s_interpret s_compile s_virtual_machine s_virtual_machine_step.
  intro ame.

  unfold run.
  Check interpreting_equals_compile_and_execute.
  rewrite (interpreting_equals_compile_and_execute interpreter compiler virtual_machine virtual_machine_step s_interpret s_compile s_virtual_machine s_virtual_machine_step ame nil).
  rewrite -> there_is_something_on_stack.
  reflexivity.
Qed.
(* ********** *)

(* Exercise 9:
   Write a Magritte-style execution function for a byte-code program
   that does not operate on natural numbers but on syntactic representations
   of natural numbers:

   Definition data_stack := list arithmetic_expression.

   * Executing (PUSH n) given s has the effect of pushing (Lit n) on s.

   * Executing ADD given s has the effect of popping two arithmetic
     expressions from s and then pushing the syntactic representation of
     their addition.

   * Executing MUL given s has the effect of popping two arithmetic
     expressions from s and then pushing the syntactic representation of
     their multiplication.

   Again, for this week's exercise,
   assume there are enough arithmetic expressions on the data stack.
   If that is not the case, just pad it up with syntactic representations
   of zero.

*)
Definition mdata_stack := list arithmetic_expression.

(* TODO:
 * - Unit tests
 * - Specification
 * - Proof that unit tests adhere to specification.
 *)
Fixpoint mexec_byte_code_instruction
  (instr : byte_code_instruction)
  (ds : mdata_stack) : mdata_stack :=
  match instr with
    | PUSH n =>
      ((Lit n) :: ds)
    | ADD =>
      match ds with
        | (nil) =>
          ((Lit 0) :: nil)
        | (a :: nil) =>
          ((Plus a (Lit 0)) :: nil)
        | (a :: b :: ds') =>
          ((Plus b a) :: ds')
      end
    | MUL =>
      match ds with
        | (nil) =>
          ((Lit 0) :: nil)
        | (a :: nil) =>
          ((Lit 0) :: nil)
        | (a :: b :: ds') =>
          ((Times b a) :: ds')
      end
end.

(* TODO: Check against unit tests *)
(* TODO: Prove that it furfills specification *)

Lemma mexec_add_a_b :
  forall (a b : arithmetic_expression)
         (ds : mdata_stack),
  mexec_byte_code_instruction ADD (a :: b :: ds) = ((Plus b a) :: ds).
Proof.
  unfold_tactic mexec_byte_code_instruction.
Qed.

Lemma mexec_mul_a_b :
  forall (a b : arithmetic_expression)
         (ds : mdata_stack),
  mexec_byte_code_instruction MUL (a :: b :: ds) = ((Times b a) :: ds).
Proof.
  unfold_tactic mexec_byte_code_instruction.
Qed.

(* TODO:
 * - Unit tests
 * - Specification
 * - Proof that unit tests adhere to specification.
 *)
Fixpoint mexec_byte_code_program
         (program : byte_code_program)
         (ds : mdata_stack) : mdata_stack :=
  match program with
    | (nil) =>
        ds
    | (instr :: program') =>
        mexec_byte_code_program program' (mexec_byte_code_instruction instr ds)
  end.

(* TODO: Check against unit tests *)
(* TODO: Prove that it furfills specification *)

Lemma mexec_nil_does_nothing :
  forall (ds : mdata_stack),
    mexec_byte_code_program nil ds = ds.
Proof.
  unfold_tactic mexec_byte_code_program.
Qed.

Lemma mexec_program_ic :
  forall (instr : byte_code_instruction)
         (program' : byte_code_program)
         (ds : mdata_stack),
    mexec_byte_code_program (instr :: program') ds = mexec_byte_code_program program' (mexec_byte_code_instruction instr ds).
Proof.
  unfold_tactic mexec_byte_code_program.
Qed.

Lemma mexec_lit :
  forall (n : nat)
         (ds : mdata_stack),
  mexec_byte_code_instruction (PUSH n) ds = (Lit n) :: ds.
Proof.
  unfold_tactic mexec_byte_code_instruction.
Qed.

Lemma mexecuting_p1p2_equals_mexecuting_p1_then_p2 :
  forall (p1 p2 : byte_code_program)
         (ds : mdata_stack),
    mexec_byte_code_program (p1 ++ p2) ds = mexec_byte_code_program p2 (mexec_byte_code_program p1 ds).
Proof.
  intros p1 p2.
  induction p1 as [ | instr program' IHp'].
    intro ds.
    rewrite -> (unfold_append_bc p2).
    rewrite -> (mexec_nil_does_nothing ds).
    reflexivity.

    intro ds.
    Check unfold_append_ic.
    rewrite -> (unfold_append_ic instr program' p2).
    rewrite -> (mexec_program_ic instr (program' ++ p2) ds).
    rewrite -> (IHp' (mexec_byte_code_instruction instr ds)).
    rewrite <- (mexec_program_ic instr program' ds).
    reflexivity.
Qed.

(* Exercise 10:
   Prove that the Magrite-style execution function from Exercise 9
   implements a decompiler that is the left inverse of the compiler
   of Exercise 6.
*)
Lemma magritte_is_decompiler :
  forall (compiler : arithmetic_expression -> byte_code_program),
    specification_of_compile compiler ->
  forall (ae : arithmetic_expression)
         (ds : mdata_stack),
  (mexec_byte_code_program (compiler ae) ds) = (ae :: ds).
Proof.
  intro compiler.
  intro s_compile.
  intro ae.
  unfold specification_of_compile in s_compile.
  destruct s_compile as [compile_lit [compile_plus compile_times]].
  induction ae.
    intro ds.
    rewrite -> (compile_lit n).
    Check mexec_program_ic.
    rewrite -> (mexec_program_ic (PUSH n) nil ds).
    rewrite -> (mexec_nil_does_nothing (mexec_byte_code_instruction (PUSH n) ds)).
    Check mexec_lit.
    rewrite -> (mexec_lit n ds).
    reflexivity.

    intro ds.
    rewrite (compile_plus ae1 ae2).
    rewrite ->2 mexecuting_p1p2_equals_mexecuting_p1_then_p2.
    rewrite (mexec_program_ic ADD nil (mexec_byte_code_program (compiler ae2) (mexec_byte_code_program (compiler ae1) ds))).
    rewrite (mexec_nil_does_nothing (mexec_byte_code_instruction ADD (mexec_byte_code_program (compiler ae2) (mexec_byte_code_program (compiler ae1) ds)))).
    rewrite (IHae1 ds).
    rewrite (IHae2 (ae1 :: ds)).
    Check mexec_add_a_b.
    rewrite (mexec_add_a_b ae2 ae1 ds).
    reflexivity.

    intro ds.
    rewrite (compile_times ae1 ae2).
    rewrite ->2 mexecuting_p1p2_equals_mexecuting_p1_then_p2.
    rewrite (mexec_program_ic MUL nil (mexec_byte_code_program (compiler ae2) (mexec_byte_code_program (compiler ae1) ds))).
    rewrite (mexec_nil_does_nothing (mexec_byte_code_instruction MUL (mexec_byte_code_program (compiler ae2) (mexec_byte_code_program (compiler ae1) ds)))).
    rewrite (IHae1 ds).
    rewrite (IHae2 (ae1 :: ds)).
    Check mexec_mul_a_b.
    rewrite (mexec_mul_a_b ae2 ae1 ds).
    reflexivity.
Qed.

(* ********** *)

(* end of week_39b_arithmetic_expressions.v *)
