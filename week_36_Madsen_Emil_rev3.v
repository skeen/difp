(* week_36c_mul.v *)
(* dIFP 2014-2015, Q1, Week 36 *)
(* Olivier Danvy <danvy@cs.au.dk> *)


(* Handin by Emil 'Skeen' Madsen 20105376 *)

(* ********** *)

Require Import Arith Bool.

Require Import unfold_tactic.

(* ********** *)

Notation "A === B" := (beq_nat A B) (at level 70, right associativity).

Definition unit_tests_for_multiplication (mul : nat -> nat -> nat) :=
  (mul 0 0 === 0)
  &&
  (mul 0 1 === 0)
  &&
  (mul 1 1 === 1)
  &&
  (mul 1 0 === 0)
  &&
  (mul 3 1 === 3)
  &&
  (mul 1 3 === 3)
  &&
  (mul 4 3 === 12)
  &&
  (mul 3 4 === 12).

(* mult is the built-in multiplication in Coq (infix notation: * ): *)
Compute (unit_tests_for_multiplication mult).
(*
     = true
     : bool
*)

(* Exercise 1: why is there a space in the comment just above
   on the right of the infix notation for multiplication?

   Because, otherwise it would be the comment termination token.
*)

(* ********** *)

Definition specification_of_multiplication (mul : nat -> nat -> nat) :=
  (forall j : nat,
    mul O j = 0)
  /\
  (forall i' j : nat,
    mul (S i') j = j + (mul i' j)).

(* ********** *)

(* For the following exercise,
   the following lemmas from the Arith library might come handy:
   plus_0_l, plus_0_r, plus_comm, and plus_assoc.
*)

Check plus_0_l.

(* Exercise:

   * show that 0 is left-absorbant for multiplication
     (aka mult_0_l in Arith)
*)
Theorem zero_is_left_absorbant_for_mul :
  forall (mul : nat -> nat -> nat),
    specification_of_multiplication mul ->
    forall i : nat, 
      mul 0 i = 0.
Proof.
  intros mul spec_mul.
  intro i.
  destruct spec_mul as [mul_b mul_i].
  apply (mul_b i).
Qed.

(*
   * show that 0 is right-absorbant for multiplication
     (aka mult_0_r in Arith)
*)
Theorem zero_is_right_absorbant_for_mul :
  forall (mul : nat -> nat -> nat),
    specification_of_multiplication mul ->
    forall i : nat, 
      mul i 0 = 0.
Proof.
  intros mul spec_mul.
  intro i.
  destruct spec_mul as [mul_b mul_i].
  induction i as [ | n IHn].
    apply (mul_b 0).

    rewrite (mul_i n 0).
    Check plus_0_l.
    rewrite (plus_0_l (mul n 0)).
    apply (IHn).
Qed.

(*
   * show that 1 is left-neutral for multiplication
     (aka mult_1_l in Arith)
*)
Theorem one_is_left_neutral_for_mul :
  forall (mul : nat -> nat -> nat),
    specification_of_multiplication mul ->
    forall i : nat, 
      mul 1 i = i.
Proof.
  intros mul spec_mul.
  intro i.
  destruct spec_mul as [mul_b mul_i].
  induction i as [ | n IHn].
    rewrite (mul_i 0 0).
    rewrite (plus_0_l (mul 0 0)).
    apply (mul_b 0).

    rewrite (mul_i 0 (S n)).
    rewrite (mul_b (S n)).
    rewrite (plus_0_r (S n)).
    reflexivity.
Qed.

Lemma one_plus_i_equals_S_i :
  forall i : nat,
    plus 1 i = (S i).
Proof.
  unfold_tactic plus.
Qed.

(*
   * show that 1 is right-neutral for multiplication
     (aka mult_1_r in Arith)
*)
Theorem one_is_right_neutral_for_mul :
  forall (mul : nat -> nat -> nat),
    specification_of_multiplication mul ->
    forall i : nat, 
      mul i 1 = i.
Proof.
  intros mul spec_mul.
  intro i.
  destruct spec_mul as [mul_b mul_i].
  induction i as [ | n IHn].
    apply (mul_b 1).

    rewrite (mul_i n 1).
    rewrite (IHn).
    apply (one_plus_i_equals_S_i n).
Qed.

(*
   * show that multiplication is commutative
     (aka mult_comm in Arith)
*)
Proposition multiplication_is_commutative :
  forall (mul : nat -> nat -> nat),
    specification_of_multiplication mul ->
    forall i j : nat,
      mul i j = mul j i.
Proof.
  intros mul spec_mul.
  intros i.
  induction i as [ | n IHn].
    intro j.
    rewrite (zero_is_left_absorbant_for_mul mul spec_mul j).
    rewrite (zero_is_right_absorbant_for_mul mul spec_mul j).
    reflexivity.

    (* This can undoubfully be done cleaner *)
    intro j.
    destruct spec_mul as [mul_b mul_i].
    rewrite (mul_i n j).
    rewrite (IHn j).
    induction j as [ | m IHm].
      rewrite (mul_b n).
      rewrite (mul_b (S n)).
      rewrite (plus_0_l 0).
      reflexivity.

      rewrite (mul_i m (S n)).
      rewrite (mul_i m n).
      rewrite <- (one_plus_i_equals_S_i m).
      rewrite (plus_assoc (1+m) n (mul m n)).
      rewrite (plus_comm (1+m) n).
      rewrite (plus_assoc n 1 m).
      rewrite <- (plus_assoc (n+1) m (mul m n)).
      rewrite (plus_comm n 1).
      rewrite (one_plus_i_equals_S_i n).
      rewrite (IHm).
      rewrite <- (mul_i m (S n)).
      reflexivity.
Qed.

(*
   * show that the specification of multiplication is unique
*)
Theorem multiplication_is_unique :
  forall (mul1 mul2 : nat -> nat -> nat), 
    specification_of_multiplication mul1 ->
    specification_of_multiplication mul2 ->
    forall i j : nat, 
      mul1 i j = mul2 i j.
Proof.
  intros mul1 mul2 spec_mul1 spec_mul2.
  intros i j.
  destruct spec_mul1 as [mul1_b mul1_i].
  destruct spec_mul2 as [mul2_b mul2_i].
  induction i as [ | n IHn].
    rewrite (mul1_b j).
    rewrite (mul2_b j).
    reflexivity.

    rewrite (mul1_i n j).
    rewrite (mul2_i n j).
    rewrite (IHn).
    reflexivity.
Qed.

(*
   * implement multiplication,
     verify that your implementation passes the unit tests, and
     prove that your implementation satisfies the specification
*)
Fixpoint mul_v1 (i j : nat) : nat :=
  match i with
      | 0 => 0
      | S i' => j + (mul_v1 i' j)
end.

Compute (unit_tests_for_multiplication mul_v1).

Lemma unfold_mul_v1_bc :
  forall j : nat,
    mul_v1 0 j = 0.
Proof.
  unfold_tactic mul_v1.
Qed.

Lemma unfold_mul_v1_ic :
  forall i' j : nat,
    mul_v1 (S i') j = j + (mul_v1 i' j).
Proof.
  unfold_tactic mul_v1.
Qed.

Theorem mul_v1_satisfies_the_specification_of_multiplication :
  specification_of_multiplication mul_v1.
Proof.
  unfold specification_of_multiplication.
  split.

  intro j.
  apply (unfold_mul_v1_bc j).

  intros i j.
  apply (unfold_mul_v1_ic i j).
Qed.

(* end of week_36c_mul.v *)