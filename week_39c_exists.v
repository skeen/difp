(* week_39c_exists.v *)
(* dIFP 2014-2015, Q1, Week 38 *)
(* Olivier Danvy <danvy@cs.au.dk> *)

(* Revised version:

   * a comment is added for "unfold beq_nat."

   * a typo is fixed in Theorem there_is_only_one_bar
*)

(* ********** *)

Require Import Arith unfold_tactic.

(* The existential quantifier is written
     exists x : type,
       expression
   in Coq (just like the universal quantifier
   is written
     forall x : type,
       expression
   in Coq).

   If it appears in a goal,
   we process it with
     exists blah.
   where "blah" is a witness of existence.
   For example, if the goal is
     exists n : nat,
       S n = 1.
   then we write
     exists 0.

   If it appears as an assumption (a hypothesis) named A,
   we process it with
     destruct A as [x H].
   which will replace A with 2 assumptions:
     x : type
     H : ...
*)

(* ********** *)

(* Example: having an existential in the goal. *)

(* The following specification
   does not uniquely specify a function,
   because it doesn't say what happens
   to odd numbers:
*)
Definition specification_of_foo (foo : nat -> nat) :=
  forall n : nat,
    foo (2 * n) = n.

(* First version:
   even numbers are mapped to their half;
   odd numbers are mapped to their half.
*)
Fixpoint foo_v0 (n : nat) : nat :=
  match n with
    | 0 => 0
    | S 0 => 0
    | S (S n') => S (foo_v0 n')
  end.

Compute (foo_v0 6, foo_v0 7, foo_v0 8, foo_v0 9).
(*   = (3, 3, 4, 4)
     : nat * nat * nat * nat
*)

Lemma unfold_foo_v0_bc0 :
  foo_v0 0 = 0.
Proof.
  unfold_tactic foo_v0.
Qed.

Lemma unfold_foo_v0_bc1 :
  foo_v0 1 = 0.
Proof.
  unfold_tactic foo_v0.
Qed.

Lemma unfold_foo_v0_ic :
  forall n' : nat,
    foo_v0 (S (S n')) = S (foo_v0 n').
Proof.
  unfold_tactic foo_v0.
Qed.

(* Second version:
   even numbers are mapped to their half;
   odd numbers are mapped to their half, plus 1.
*)
Fixpoint foo_v1 (n : nat) : nat :=
  match n with
    | 0 => 0
    | S 0 => 1
    | S (S n') => S (foo_v1 n')
  end.

Compute (foo_v1 6, foo_v1 7, foo_v1 8, foo_v1 9).
(*   = (3, 4, 4, 5)
     : nat * nat * nat * nat
*)

Lemma unfold_foo_v1_bc0 :
  foo_v1 0 = 0.
Proof.
  unfold_tactic foo_v1.
Qed.

Lemma unfold_foo_v1_bc1 :
  foo_v1 1 = 1.
Proof.
  unfold_tactic foo_v1.
Qed.

Lemma unfold_foo_v1_ic :
  forall n' : nat,
    foo_v1 (S (S n')) = S (foo_v1 n').
Proof.
  unfold_tactic foo_v1.
Qed.

(* Proof that specification_of_foo
   does not uniquely specify a function,
   with foo_v0 and foo_v1 as witness
   and any odd number as input.
*)
Theorem there_are_at_least_two_foo_s :
  exists f g : nat -> nat,
    specification_of_foo f ->
    specification_of_foo g ->
    exists n : nat,
      f n <> g n.
Proof.
  exists foo_v0.  (* Eureka. *)
  exists foo_v1.  (* Eureka. *)
  unfold specification_of_foo.
  intros H_foo_v0 H_foo_v1.
  exists 1.  (* Eureka. *)
  rewrite -> unfold_foo_v0_bc1.
  rewrite -> unfold_foo_v1_bc1.
(* beq_nat_false :
   forall x y : nat, beq_nat x y = false -> x <> y *)
  apply beq_nat_false.
  (* Both actual parameters are literals,
     so unfolding to call to beq_nat_false
     makes Coq yield the result, i.e., false. *)
  unfold beq_nat.
  reflexivity.
Qed.

(* ********** *)

(* The following specification
   uniquely specifies a function,
   because it says what happens
   both to even numbers and to odd numbers:
*)
Definition specification_of_bar (foo : nat -> nat) :=
  (forall n : nat,
     foo (2 * n) = n)
  /\
  (forall n : nat,
     foo (S (2 * n)) = n).

(* A useful lemma,
   using an existential quantifier:
*)
Proposition a_natural_number_is_even_or_it_is_odd :
  forall n : nat,
    exists m : nat,
      n = 2 * m \/ n = S (2 * m).
Proof.
  intro n.
  induction n as [ | n' [m [IHn'_even | IHn'_odd]]].

  exists 0.
  left.
  rewrite -> mult_0_r.
  reflexivity.

  rewrite -> IHn'_even.
  exists m.
  right.
  reflexivity.

  rewrite -> IHn'_odd.
  exists (S m).
  left.
  ring. (* No time to lose (Tarney Spencer Band dixit) *)
Qed.  

Theorem there_is_only_one_bar :
  forall f g : nat -> nat,
    specification_of_bar f ->
    specification_of_bar g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_bar.
  intros [Hf_even Hf_odd] [Hg_even Hg_odd].
  intro n.
  destruct (a_natural_number_is_even_or_it_is_odd n)
        as [m [Hn_even | Hn_odd]].

  rewrite -> Hn_even.
  rewrite -> Hf_even.
  rewrite -> Hg_even.
  reflexivity.

  rewrite -> Hn_odd.
  rewrite -> Hf_odd.
  rewrite -> Hg_odd.
  reflexivity.
Qed.

(* ********** *)

(* end of week_39c_exists.v *)
