(* week_40_exercises.v *)
(* dIFP 2014-2015, Q1, Week 40 *)
(* Olivier Danvy <danvy@cs.au.dk> *)

(* Handin by;
 * Emil 'Skeen' Madsen - 20105376
 * Søren Krogh         - 20105661
 *)

(* ********** *)

Require Import Arith Bool List unfold_tactic.

(* ********** *)
    
Notation "A =n= B" := (beq_nat A B) (at level 70, right associativity).

(* ********** *)

(* Data type of binary trees of natural numbers: *)

Inductive binary_tree_nat : Type :=
  | Leaf : nat -> binary_tree_nat
  | Node : binary_tree_nat -> binary_tree_nat -> binary_tree_nat.

(* Specification of the number of leaves *)
(* Unit tests in week_40a *)
(* Uniqueness proven in week_40a *)
Definition specification_of_number_of_leaves (number_of_leaves : binary_tree_nat -> nat) :=
  (forall n : nat,
     number_of_leaves (Leaf n) = 1)
  /\
  (forall t1 t2 : binary_tree_nat,
     number_of_leaves (Node t1 t2) = number_of_leaves t1 + number_of_leaves t2).

(* Consider the following alternative definitions: *)
(* Note; The definitions assumed unit tested *)

(* Exercise : Do these two definitions satisfy the specification of number_of_leaves? *)

(* First implementation *)
Fixpoint number_of_leaves_acc' (t : binary_tree_nat) (a : nat) : nat :=
  match t with
    | Leaf n =>
      1 + a
    | Node t1 t2 =>
      (number_of_leaves_acc' t2 (number_of_leaves_acc' t1 a))
  end.

Definition number_of_leaves_v1' (t : binary_tree_nat) : nat :=
  number_of_leaves_acc' t 0.

(* Unfold lemmas *)
Lemma leaves_acc_bc :
  forall n : nat,
    forall a : nat,
    number_of_leaves_acc' (Leaf n) a = (S a).
Proof.
  unfold_tactic number_of_leaves_acc'.
Qed.

Lemma leaves_acc_ic :
  forall t1 t2 : binary_tree_nat,
    forall a : nat,
    number_of_leaves_acc' (Node t1 t2) a = (number_of_leaves_acc' t2 (number_of_leaves_acc' t1 a)).
Proof.
  unfold_tactic number_of_leaves_acc'.
Qed. 

(* Appending an environment is okay.
 * Needed to proove that the implementation satisfy the specification
 *)
Lemma leaves_acc_append_a_okay :
  forall t : binary_tree_nat,
    forall a : nat,
   number_of_leaves_acc' t a =
   number_of_leaves_acc' t 0 + a.
Proof.
  intros t.
  induction t as [ | t1 IHt1 t2 IHt2].
    intro a.
    rewrite ->2 leaves_acc_bc.
    ring.

    intro a.
    rewrite ->2 leaves_acc_ic.
    rewrite -> IHt1.
    rewrite -> IHt2.
    rewrite -> plus_assoc.
    rewrite <- IHt2.
    reflexivity.
Qed.

(* Proof that the implementation satisfy specification *)
Lemma leaves_v1'_satisfy_specification_of_number_of_leaves :
  specification_of_number_of_leaves number_of_leaves_v1'.
Proof.
  unfold specification_of_number_of_leaves.
  split.
    intro n.
    unfold number_of_leaves_v1'.
    apply leaves_acc_bc.
    
    intros t1 t2.
    unfold number_of_leaves_v1'.
    rewrite -> leaves_acc_ic.
    rewrite -> plus_comm.
    rewrite -> leaves_acc_append_a_okay.
    reflexivity.
Qed.

(* Second implementation *)
Fixpoint number_of_leaves_cps' (ans : Type) (t : binary_tree_nat) (k : nat -> ans) : ans :=
  match t with
    | Leaf n =>
      k 1
    | Node t1 t2 =>
      number_of_leaves_cps'
        ans
        t2
        (fun n2 => number_of_leaves_cps'
                     ans
                     t1
                     (fun n1 => k (n1 + n2)))
  end.

Definition number_of_leaves_v3' (t : binary_tree_nat) : nat :=
  number_of_leaves_cps' nat t (fun n => n).

(* Unfold lemmas *)
Lemma leaves_cps_bc :
  forall ans : Type,
  forall n : nat,
  forall k : (nat -> ans),
    number_of_leaves_cps' ans (Leaf n) k = k 1.
Proof.
  unfold_tactic number_of_leaves_cps'.
Qed.

Lemma leaves_cps_ic :
  forall ans : Type,
  forall t1 t2 : binary_tree_nat,
  forall k : (nat -> ans),
    number_of_leaves_cps' ans (Node t1 t2) k = 
    number_of_leaves_cps'
      ans
      t2
      (fun n2 => number_of_leaves_cps'
                   ans
                   t1
                   (fun n1 => k (n1 + n2))).
Proof.
  unfold_tactic number_of_leaves_cps'.
Qed. 

(* Lemma about resetting the continuation: *)
Lemma about_leaves_cps :
  forall (ans: Type)
         (t : binary_tree_nat)
         (k : nat -> ans),
    number_of_leaves_cps' ans t k = k (number_of_leaves_cps' nat t (fun a => a)).
Proof.
  intro ans.
  intro t.
  revert ans.
  induction t as [ | t1 IHt1 t2 IHt2].
    intro ans.
    intro k.
    rewrite ->2 leaves_cps_bc.
    reflexivity.

    intro ans.
    intro k.
    rewrite ->2 leaves_cps_ic.
    rewrite -> IHt2.
    rewrite -> IHt1.
    symmetry.
    rewrite -> IHt2.
    rewrite -> IHt1.
    reflexivity.
Qed.

(* Proof that the implementation satisfy specification *)
Lemma leaves_v3'_satisfy_the_specification :
  specification_of_number_of_leaves number_of_leaves_v3'.
Proof.
  unfold specification_of_number_of_leaves.
  split.
    intro n.
    unfold number_of_leaves_v3'.
    rewrite -> leaves_cps_bc.
    reflexivity.

    intros t1 t2.
    unfold number_of_leaves_v3'.
    rewrite -> leaves_cps_ic.
    rewrite ->2 about_leaves_cps.
    reflexivity.
Qed.

(* Write a function that naively computes the product (i.e., the
  multiplication) of all the numbers in the leaves of a binary tree. *)
(* Done in the form of;
 * Unit tests
 * Specification
 * Proof that unit tests adhere to specification
 * Uniqueness of Specification
 * Implementation
 * Test implementation against unit tests
 * Proof that implementation satisfy specification
 *)

(* Unit tests *)
Definition unit_test_mult_tree (f : binary_tree_nat -> nat) :=
  (f (Leaf 1) =n= 1)
  &&
  (f (Leaf 2) =n= 2)
  &&
  (f (Node (Leaf 1) (Leaf 2)) =n= 2)
  &&
  (f (Node (Leaf 2) (Leaf 2)) =n= 4)
  &&
  (f (Node (Leaf 5) (Leaf 5)) =n= 25)
  &&
  (f (Node (Node (Leaf 1) (Leaf 2)) (Leaf 3)) =n= 6)
  &&
  (f (Node (Node (Leaf 2) (Leaf 2)) (Leaf 2)) =n= 8)
  &&
  (f (Node (Node (Leaf 2) (Leaf 3)) (Leaf 5)) =n= 30)
.

(* Specification of Multiplication *)
Definition specification_of_multiply_tree (f : binary_tree_nat -> nat) :=
  (forall n : nat,
     (f (Leaf n)) = n)
  /\
  (forall t1 t2 : binary_tree_nat,
      (f (Node t1 t2)) = (f t1) * (f t2)).

(* Unit tests adhere to specification *)
Lemma unit_tests_mult_tree_adhere_to_specification :
  forall (f : binary_tree_nat -> nat),
    specification_of_multiply_tree f ->
    unit_test_mult_tree f = true.
Proof.
  intro f.
  unfold specification_of_multiply_tree.
  intros [f_bc f_ic].
  unfold unit_test_mult_tree.
  (* Apply the maximum amount of times *)
  rewrite ->? f_ic.
  rewrite ->? f_bc.
  ring.
Qed.

(* Proof of Uniqueness *)
Lemma there_is_only_one_multiply_tree :
  forall f g : binary_tree_nat -> nat,
    specification_of_multiply_tree f ->
    specification_of_multiply_tree g ->
    forall t : binary_tree_nat,
      f t = g t.
Proof.
  intros f g.
  unfold specification_of_multiply_tree.
  intros [f_bc f_ic [g_bc g_ic]].
  intro t.
  induction t as [ | t1 IHt1 t2 IHt2].
     rewrite -> f_bc.
     rewrite -> g_bc.
     reflexivity.

     rewrite -> f_ic.
     rewrite -> g_ic.
     rewrite -> IHt1.
     rewrite -> IHt2.
     reflexivity.
Qed.

(* Naive specification based implementation *)
Fixpoint multiply_tree (t : binary_tree_nat) : nat :=
  match t with
    | Leaf n =>
      n
    | Node t1 t2 =>
      (multiply_tree t1) * (multiply_tree t2)
  end.

(* Assert that the implementation passes unit tests *)
Compute unit_test_mult_tree multiply_tree.

(* Unfold lemmas *)
Lemma multiply_tree_bc :
  forall n : nat,
    (multiply_tree (Leaf n)) = n.
Proof.
  unfold_tactic multiply_tree.
Qed.

Lemma multiply_tree_ic :
  forall t1 t2 : binary_tree_nat,
    (multiply_tree (Node t1 t2)) = (multiply_tree t1) * (multiply_tree t2).
Proof.
  unfold_tactic multiply_tree.
Qed.

(* Proof that the implementation satisfy specification *)
Lemma multiply_tree_satisfy_specification_of_multiply_tree :
  specification_of_multiply_tree multiply_tree.
Proof.
  unfold specification_of_multiply_tree.
  split.
     exact multiply_tree_bc.
     exact multiply_tree_ic.
Qed.

(* Devise a more efficient version that exploits the fact;
 * that 0 is absorbant for multiplication.
 *)
(* The below version is using an accumulator, and is zero absorbant *)
Fixpoint multiply_tree_absorb2_work (t : binary_tree_nat) (a : nat) : nat :=
  match a with
    | 0 => 0
    | _ => match t with
             | Leaf n =>
               n * a
             | Node t1 t2 =>
               (multiply_tree_absorb2_work t2 (multiply_tree_absorb2_work t1 a))
           end
  end.

Fixpoint multiply_tree_absorb2 (t : binary_tree_nat) : nat :=
  multiply_tree_absorb2_work t 1.

(* Assert that the implementation passes unit tests *)
Compute unit_test_mult_tree multiply_tree_absorb2.

(* Unfold lemmas *)
(* Special cases for a = 0 *)
Lemma multiply_tree_absorb2_bc_empty :
  forall n : nat,
    (multiply_tree_absorb2_work (Leaf n) 0) = 0.
Proof.
  unfold_tactic multiply_tree_absorb2_work.
Qed.

Lemma multiply_tree_absorb2_ic_empty :
  forall t1 t2 : binary_tree_nat,
    (multiply_tree_absorb2_work (Node t1 t2) 0) = 0.
Proof.
  unfold_tactic multiply_tree_absorb2_work.
Qed.

(* (* Lemma that if a = 0, then the result will always be 0 *)
Lemma multiply_tree_absorb2_acc_empty :
  forall t : binary_tree_nat,
    multiply_tree_absorb2_work t 0 = 0.
Proof.
  intro t.
  case t.
    intro n.
    rewrite -> multiply_tree_absorb2_bc_empty.
    reflexivity.

    intros t1 t2.
    rewrite -> multiply_tree_absorb2_ic_empty.
    reflexivity.
Qed.
*)

(* Unfold lemmas for a <> 0 *)
Lemma multiply_tree_absorb2_bc :
  forall n : nat,
    forall a : nat,
    (multiply_tree_absorb2_work (Leaf n) (S a)) = n * (S a).
Proof.
  unfold_tactic multiply_tree_absorb2_work.
Qed.

Lemma multiply_tree_absorb2_ic :
  forall t1 t2 : binary_tree_nat,
    forall a : nat,
    (multiply_tree_absorb2_work (Node t1 t2) (S a))
    = (multiply_tree_absorb2_work t2 (multiply_tree_absorb2_work t1 (S a))).
Proof.
  unfold_tactic multiply_tree_absorb2_work.
Qed.

(* A real unfold lemma, it unfolds literally!
 * To avoid (fix x) application unfolding.
 *)
Lemma unfold_multiply_tree_absorb2 :
  forall t : binary_tree_nat,
   multiply_tree_absorb2 t = multiply_tree_absorb2_work t 1.
Proof.
  intro t.
  case t.
    unfold multiply_tree_absorb2.
    reflexivity.

    unfold multiply_tree_absorb2.
    reflexivity.
Qed.

(* Appending an environment is okay.
 * Needed to proove that the implementation satisfy the specification
 *)
Lemma multiply_tree_absorb2_append_a_okay :
  forall t : binary_tree_nat,
    forall a : nat,
      multiply_tree_absorb2_work t a =
      multiply_tree_absorb2_work t 1 * a.
Proof.
  intros t.
  induction t as [ | t1 IHt1 t2 IHt2].
    intro a.
    case a.
      rewrite -> multiply_tree_absorb2_bc_empty.
      ring.

      intro a'.
      rewrite ->2 multiply_tree_absorb2_bc.
      ring.

    intro a.
    case a.
      rewrite -> multiply_tree_absorb2_ic_empty.
      ring.

      intro a'.
      rewrite ->2 multiply_tree_absorb2_ic.
      rewrite -> IHt1.
      rewrite -> IHt2.
      rewrite -> mult_assoc.
      rewrite <- IHt2.
      reflexivity.
Qed.

(* Proof that the implementation satisfy specification *)
Lemma multiply_tree_absorb2_satisfy_specification_of_multiply_tree :
  specification_of_multiply_tree multiply_tree_absorb2.
Proof.
  unfold specification_of_multiply_tree.
  split.
     intro n.
     unfold multiply_tree_absorb2.
     rewrite -> multiply_tree_absorb2_bc.
     ring.

     intros t1 t2.
     rewrite ->3 unfold_multiply_tree_absorb2.
     rewrite -> multiply_tree_absorb2_ic.
     rewrite -> multiply_tree_absorb2_append_a_okay.
     rewrite -> mult_comm.
     reflexivity.
Qed.

(* Prove that the naive function and the more efficient version
  are equivalent in that they compute the same result, for any given
  tree. *)
Lemma naive_and_zero_absorbant_are_equivalent :
  forall t : binary_tree_nat,
    multiply_tree t = multiply_tree_absorb2 t.
Proof.
  intro t.
  Check there_is_only_one_multiply_tree.
  exact (there_is_only_one_multiply_tree
           multiply_tree
           multiply_tree_absorb2
           multiply_tree_satisfy_specification_of_multiply_tree
           multiply_tree_absorb2_satisfy_specification_of_multiply_tree
           t).
Qed.

(* For any binary tree,
  how would you compare its number of leaves and its number of nodes?
  Is there a relation? *)
(* Yes there is; there will always be one additional leaf,
 * compared to the number of internal nodes
 * This is given by the two edge cases, i.e.;
 * - The perfectly balanced binary tree
 * - The perfectly imbalanced binary tree (chain)
 * Both of these holds the above
 *)

(* If so, could you formalize it in Coq? *)
(* Required below *)
Lemma S_is_plus_one :
  forall x : nat,
    S x = 1 + x.
Proof.
  intro x.
  ring.
Qed.

(* Specification of the number of nodes *)
Definition specification_of_number_of_nodes (number_of_nodes : binary_tree_nat -> nat) :=
  (forall n : nat,
     number_of_nodes (Leaf n) = 0)
  /\
  (forall t1 t2 : binary_tree_nat,
     number_of_nodes (Node t1 t2) = S (number_of_nodes t1 + number_of_nodes t2)).

Lemma leaves_relating_to_nodes :
  forall f g : binary_tree_nat -> nat,
    specification_of_number_of_leaves f ->
    specification_of_number_of_nodes g ->
    forall t : binary_tree_nat,
      (g t) + 1 = (f t).
Proof.
  intros f g.
  unfold specification_of_number_of_leaves.
  intros [f_bc f_ic].
  unfold specification_of_number_of_nodes.
  intros [g_bc g_ic].
  intro t.
  induction t as [ | t1 IHt1 t2 IHt2].
    rewrite -> f_bc.
    rewrite -> g_bc.
    ring.

    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> S_is_plus_one.
    rewrite -> plus_assoc.
    rewrite <- IHt1.
    rewrite <- IHt2.
    ring.
Qed.

(* Prove that composing swap_v0 with itself yields the identity function. *)
Fixpoint swap_ds (t : binary_tree_nat) : binary_tree_nat :=
  match t with
    | Leaf n => Leaf n
    | Node t1 t2 => Node (swap_ds t2) (swap_ds t1)
  end.

Definition swap_v0 (t : binary_tree_nat) : binary_tree_nat :=
  swap_ds t.

Lemma swap_bc :
  forall n : nat,
    swap_ds (Leaf n) = Leaf n.
Proof.
  unfold_tactic swap_ds.
Qed.

Lemma swap_ic :
  forall t1 t2 : binary_tree_nat,
    swap_ds (Node t1 t2) = Node (swap_ds t2) (swap_ds t1).
Proof.
  unfold_tactic swap_ds.
Qed.

Lemma swap_ds_swap_v0_who_cares :
  forall t : binary_tree_nat,
    swap_ds t = swap_v0 t.
Proof.
  unfold_tactic swap_v0.
Qed.

Lemma swap_identity : (* with who? *)
  forall t : binary_tree_nat,
    swap_v0 (swap_v0 t) = t.
Proof.
  intro t.
  induction t as [ | t1 IHt1 t2 IHt2].
    rewrite ->2 swap_bc.
    reflexivity.

    rewrite ->2 swap_ic.
    rewrite -> swap_ds_swap_v0_who_cares.
    rewrite -> (swap_ds_swap_v0_who_cares t1).
    rewrite -> IHt1.
    rewrite -> swap_ds_swap_v0_who_cares.
    rewrite -> (swap_ds_swap_v0_who_cares t2).
    rewrite -> IHt2.
    reflexivity.
Qed.

(* What is the result of applying flatten_v0
   to the result of applying swap_v0 to a tree?
*)
(* A reversed list *)

(* Formalize this in Coq. *)
(* Specification from week_40c *)
Definition specification_of_flatten (flatten : binary_tree_nat -> list nat) :=
  (forall n : nat,
     flatten (Leaf n) = n :: nil)
  /\
  (forall t1 t2 : binary_tree_nat,
     flatten (Node t1 t2) = (flatten t1) ++ (flatten t2)).

(* Specification derived from week_37b *)
Definition specification_of_reverse (T : Type) (reverse : list T -> list T) :=
    (reverse nil = nil)
    /\
    (forall (x : T) (xs' : list T),
       reverse (x :: xs') = (reverse xs') ++ (x :: nil)).

(* Specification derived from fixpoint *)
Definition specification_of_swap (swap : binary_tree_nat -> binary_tree_nat) :=
  (forall n : nat,
     swap (Leaf n) = Leaf n)
  /\
  (forall t1 t2 : binary_tree_nat,
     swap (Node t1 t2) = Node (swap t2) (swap t1)).

(* Proposition from week_37b *)
Proposition reverse_preserves_append_sort_of :
  forall (T : Type)
         (reverse : list T -> list T),
    specification_of_reverse T reverse ->
    forall xs ys : list T,
      reverse (xs ++ ys) = (reverse ys) ++ (reverse xs).
Proof.
  intro T.
  intro reverse.
  unfold specification_of_reverse.
  intros [reverse_bc reverse_ic].
  intro xs.
  induction xs as [ | xs' xss' IHxss'].
    intro ys.
    rewrite -> (app_nil_l).
    rewrite -> (reverse_bc).
    rewrite -> (app_nil_r).
    reflexivity.

    intro ys.
    rewrite <- (app_comm_cons).
    rewrite -> (reverse_ic).
    rewrite -> (IHxss').
    rewrite <- (app_assoc).
    rewrite <- (reverse_ic).
    reflexivity.
Qed.

Theorem about_flattening_a_swapped_binary_tree :
  forall flatten : binary_tree_nat -> list nat,
    specification_of_flatten flatten ->
    forall reverse : list nat -> list nat,
      specification_of_reverse nat reverse ->
      forall swap : binary_tree_nat -> binary_tree_nat,
        specification_of_swap swap ->
        forall t : binary_tree_nat,
          flatten (swap t) = reverse (flatten t).
Proof.
  intro flatten.
  unfold specification_of_flatten.
  intros [flatten_bc flatten_ic].

  intro reverse.
  intro s_reverse.
  assert (s_reverse' := s_reverse).
  revert s_reverse'.
  unfold specification_of_reverse.
  intros [reverse_bc reverse_ic].

  intro swap.
  unfold specification_of_swap.
  intros [swap_bc swap_ic].

  intro t.
  induction t as [ | t1 IHt1 t2 IHt2].
    rewrite -> swap_bc.
    rewrite -> flatten_bc.
    rewrite -> reverse_ic.
    rewrite -> reverse_bc.
    rewrite -> app_nil_l.
    reflexivity.

    rewrite -> swap_ic.
    rewrite -> flatten_ic.
    rewrite -> IHt1.
    rewrite -> IHt2.
    rewrite -> flatten_ic.
    rewrite -> reverse_preserves_append_sort_of.
    reflexivity.

    exact s_reverse.
Qed.

(* ********** *)

(* end of week_40c_flatten.v *)
