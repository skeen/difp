Lemma Identitiy :
  forall A : Prop,
    A -> A.
Proof.
  intro A. 
  intro H_A.
  apply H_A.
Qed.

Lemma Identitiy' :
  forall B : Prop,
    B -> B.
Proof.
  intro B.
  intro H_B.
  apply H_B.
Qed.

Lemma Identitiy'' :
  forall C : Prop,
    C -> C.
Proof.
  intro C.
  intro H_C.
  apply H_C.
Qed.
