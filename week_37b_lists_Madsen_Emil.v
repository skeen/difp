(* week_37b_lists.v *)
(* dIFP 2014-2015, Q1, Week 37 *)
(* Olivier Danvy <danvy@cs.au.dk> *)


(* Handin by Emil 'Skeen' Madsen 20105376 *)

(* ********** *)

(* New tactics:
   - assert (to declare a new hypothesis)
   - clear (to garbage collect the hypotheses).
*)

Require Import unfold_tactic.

Require Import Arith Bool List.

(* The goal of this file is to study lists:
   Infix :: is cons, and nil is the empty list.
*)

Compute 3 :: 2 :: 1 :: nil.
(*
     = 3 :: 2 :: 1 :: nil
     : list nat
*)

(* ********** *)

Lemma f_equal_S :
  forall x y : nat,
    x = y -> S x = S y.
Proof.
  intros x y.
  intro H_xy.
  rewrite -> H_xy.
  reflexivity.
Qed.

(* ********** *)

(* The length of a list: *)

Notation "A === B" := (beq_nat A B) (at level 70, right associativity).

Definition unit_tests_for_length_nat (length : list nat -> nat) :=
  (length nil === 0)
  &&
  (length (1 :: nil) === 1)
  &&
  (length (2 :: 1 :: nil) === 2)
  &&
  (length (3 :: 2 :: 1 :: nil) === 3)
  .

Definition unit_tests_for_length_bool (length : list bool -> nat) :=
  (length nil === 0)
  &&
  (length (true :: nil) === 1)
  &&
  (length (true :: true :: nil) === 2)
  &&
  (length (true :: true :: true :: nil) === 3)
  .

Definition specification_of_length (T : Type) (length : list T -> nat) :=
  (length nil = 0)
  /\
  (forall (x : T) (xs' : list T),
     length (x :: xs') = S (length xs')).

Theorem there_is_only_one_length :
  forall (T : Type) (length_1 length_2 : list T -> nat),
    specification_of_length T length_1 ->
    specification_of_length T length_2 ->
    forall xs : list T,
      length_1 xs = length_2 xs.
Proof.
  intros T length_1 length_2.

  unfold specification_of_length.
  intros [Hbc1 Hic1] [Hbc2 Hic2].

  intro xs.
  induction xs as [ | x xs' IHxs'].

  rewrite -> Hbc1.
  rewrite -> Hbc2.
  reflexivity.

  rewrite (Hic1 x xs').
  rewrite (Hic2 x xs').
  rewrite -> IHxs'.
  reflexivity.
Qed.

(* ***** *)

(* A first implementation of length: *)

Fixpoint length_ds (T : Type) (xs : list T) : nat :=
  match xs with
    | nil => 0
    | x :: xs' => S (length_ds T xs')
  end.

Definition length_v1 (T : Type) (xs : list T) : nat :=
  length_ds T xs.

Compute unit_tests_for_length_nat (length_v1 nat).
(*
     = true
     : bool
*)

Compute unit_tests_for_length_bool (length_v1 bool).
(*
     = true
     : bool
*)

(* The canonical unfold lemmas: *)

Lemma unfold_length_ds_base_case :
  forall T : Type,
    length_ds T nil = 0.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic length_ds.
Qed.

Lemma unfold_length_ds_induction_case :
  forall (T : Type) (x : T) (xs' : list T),
    length_ds T (x :: xs') = S (length_ds T xs').
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic length_v1.
Qed.

Proposition length_v1_fits_the_specification_of_length :
  forall T : Type,
    specification_of_length T (length_v1 T).
Proof.
  intro T.
  unfold specification_of_length.
  split.

  apply (unfold_length_ds_base_case T).

  apply (unfold_length_ds_induction_case T).
Qed.

(* ***** *)

(* A second implementation of length: *)

Fixpoint length_acc (T : Type) (xs : list T) (a : nat) : nat :=
  match xs with
    | nil => a
    | x :: xs' => length_acc T xs' (S a)
  end.

Definition length_v2 (T : Type) (xs : list T) : nat :=
  length_acc T xs 0.

Compute unit_tests_for_length_nat (length_v2 nat).
(*
     = true
     : bool
*)

Compute unit_tests_for_length_bool (length_v2 bool).
(*
     = true
     : bool
*)

(* The canonical unfold lemmas: *)

Lemma unfold_length_acc_base_case :
  forall (T : Type) (a : nat),
    length_acc T nil a = a.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic length_acc.
Qed.

Lemma unfold_length_acc_induction_case :
  forall (T : Type) (x : T) (xs' : list T) (a : nat),
    length_acc T (x :: xs') a = length_acc T xs' (S a).
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic length_acc.
Qed.

(* A useful lemma (Eureka): *)

Lemma one_plus_i_equals_S_i :
  forall i : nat,
    plus 1 i = (S i).
Proof.
  unfold_tactic plus.
Qed.

Lemma about_length_acc_helper :
  forall (T : Type) (xs : list T) (a : nat),
    length_acc T xs (S a) = (length_acc T xs a) + 1.
Proof.
  intro t.
  intro xs.
  induction xs as [ | x' xs' IHxs'].
    intro a.
    rewrite -> (unfold_length_acc_base_case t (S a)).
    rewrite -> (unfold_length_acc_base_case t a).
    rewrite -> (plus_comm a 1).
    rewrite -> (one_plus_i_equals_S_i a).
    reflexivity.

    intro a.
    rewrite -> (unfold_length_acc_induction_case t x' xs' a).
    rewrite <- (IHxs').
    rewrite -> (unfold_length_acc_induction_case t x' xs' (S a)).
    reflexivity.
Qed.

Lemma about_length_acc :
  forall (T : Type) (xs : list T) (a : nat),
    length_acc T xs a = (length_acc T xs 0) + a.
Proof.
  intro t.
  intro xs.
  intro a.
  induction xs as [ | x' xs' IHxs'].
     rewrite -> (unfold_length_acc_base_case t a).
     rewrite -> (unfold_length_acc_base_case t 0).
     rewrite -> (plus_0_l a).
     reflexivity.

     rewrite -> (unfold_length_acc_induction_case t x' xs' a).
     rewrite -> (unfold_length_acc_induction_case t x' xs' 0).
     rewrite -> (about_length_acc_helper t xs' a).
     rewrite -> (IHxs').
     rewrite -> (about_length_acc_helper t xs' 0).
     rewrite -> (plus_comm (length_acc t xs' 0) a).
     rewrite -> (plus_comm (a + length_acc t xs' 0) 1).
     rewrite -> (plus_assoc 1 a (length_acc t xs' 0)).
     rewrite -> (plus_comm (length_acc t xs' 0) 1).
     rewrite -> (plus_comm (1 + length_acc t xs' 0) a).
     rewrite -> (plus_assoc a 1 (length_acc t xs' 0)).
     rewrite -> (plus_comm 1 a).
     reflexivity.
Qed.

Proposition length_v2_fits_the_specification_of_length :
  forall T : Type,
    specification_of_length T (length_v2 T).
Proof.
  intro t.
  unfold specification_of_length.
  split.
    unfold length_v2.
    rewrite -> (unfold_length_acc_base_case t 0).
    reflexivity.

    intros x xs'.
    unfold length_v2.
    rewrite -> (unfold_length_acc_induction_case t x xs' 0).
    rewrite -> (about_length_acc t xs' 1).
    rewrite -> (plus_comm (length_acc t xs' 0) 1).
    rewrite -> (one_plus_i_equals_S_i (length_acc t xs' 0)).
    reflexivity.
Qed.

(* ********** *)

Fixpoint equal_list_nat (xs ys : list nat) :=
  match xs with
    | nil =>
      match ys with
        | nil => true
        | y :: ys' => false
      end
    | x :: xs' =>
      match ys with
        | nil => false
        | y :: ys' => (beq_nat x y) && (equal_list_nat xs' ys')
      end
  end.

(* ********** *)

(* Concatenating two lists: *)

Definition unit_tests_for_append_nat (append : list nat -> list nat -> list nat) :=
  (equal_list_nat (append nil
                          nil)
                  nil)
  &&
  (equal_list_nat (append (1 :: nil)
                          nil)
                  (1 :: nil))
  &&
  (equal_list_nat (append nil
                          (1 :: nil))
                  (1 :: nil))
  &&
  (equal_list_nat (append (1 :: 2 :: nil)
                          (3 :: 4 :: 5 :: nil))
                  (1 :: 2 :: 3 :: 4 :: 5 :: nil))
  .

Definition specification_of_append (T : Type) (append : list T -> list T -> list T) :=
  (forall ys : list T,
     append nil ys = ys)
  /\
  (forall (x : T) (xs' ys : list T),
     append (x :: xs') ys = x :: (append xs' ys)).

Theorem there_is_only_one_append :
  forall (T : Type) (append_1 append_2 : list T -> list T -> list T),
    specification_of_append T append_1 ->
    specification_of_append T append_2 ->
    forall xs ys : list T,
      append_1 xs ys = append_2 xs ys.
Proof.
  intro t.
  intros append_1 append_2.
  intros s_append_1 s_append_2.
  intros xs ys.

  unfold specification_of_append in s_append_1.
  destruct s_append_1 as [append_1b append_1i].

  unfold specification_of_append in s_append_2.
  destruct s_append_2 as [append_2b append_2i].
  induction xs as [ | x' xs' IHxs'].
    rewrite -> (append_1b ys).
    rewrite -> (append_2b ys).
    reflexivity.

    rewrite -> (append_1i x' xs' ys).
    rewrite -> (IHxs').
    rewrite <- (append_2i x' xs' ys).
    reflexivity.
Qed.

Fixpoint append_ds (T : Type) (xs ys : list T) : list T :=
  match xs with
    | nil => ys
    | x :: xs' => x :: append_ds T xs' ys
  end.

Definition append_v1 (T : Type) (xs ys : list T) : list T :=
  append_ds T xs ys.

Compute unit_tests_for_append_nat (append_v1 nat).

Lemma unfold_append_v1_base_case :
  forall (T : Type) (ys : list T),
    append_ds T nil ys = ys.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic append_ds.
Qed.

Lemma unfold_append_v1_induction_case :
  forall (T : Type) (x : T) (xs' ys : list T),
    append_ds T (x :: xs') ys = x :: append_ds T xs' ys.
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic append_ds.
Qed.

Proposition append_v1_fits_the_specification_of_append :
  forall T : Type,
    specification_of_append T (append_v1 T).
Proof.
  intro t.
  unfold specification_of_append.
  split.
    intro ys.
    unfold append_v1.
    rewrite -> (unfold_append_v1_base_case t ys).
    reflexivity.
    
    intros x xs' ys.
    unfold append_v1.
    rewrite -> (unfold_append_v1_induction_case t x xs' ys).
    reflexivity.
Qed.

(* ********** *)

(* Properties:
     for all ys, append nil ys = ys
     for all xs, append xs nil = xs
     for all xs ys zs,
       append (append xs ys) zs = append xs (append ys zs)
     for all xs ys,
       length (append xs ys) = (length xs) + (length ys)
*)

Notation "A ==== B" := (equal_list_nat A B) (at level 70, right associativity).

Definition unit_tests_for_append_nat_properties (append : list nat -> list nat -> list nat) :=
(* for all ys, append nil ys = ys *)
  (append nil nil ==== nil)
  &&
  (append nil (1 :: nil) ==== (1 :: nil))
  &&
  (append nil (2 :: 1 :: nil) ==== (2 :: 1 :: nil))
  &&
  (append nil (3 :: 2 :: 1 :: nil) ==== (3 :: 2 :: 1 :: nil))
  &&
(* for all xs, append xs nil = xs *)
  (append nil nil ==== nil)
  &&
  (append (1 :: nil) nil ==== (1 :: nil))
  &&
  (append (2 :: 1 :: nil) nil ==== (2 :: 1 :: nil))
  &&
  (append (3 :: 2 :: 1 :: nil) nil ==== (3 :: 2 :: 1 :: nil))
  &&
(* for all xs ys zs,
       append (append xs ys) zs = append xs (append ys zs) *)
  (append (append nil nil) nil ==== append nil (append nil nil))
  &&
  (append (append (1 :: nil) (2 :: nil)) nil ==== append (1 :: nil) (append (2 :: nil) nil))
  &&
  (append (append (1 :: nil) (2 :: nil)) (3 :: nil) ==== append (1 :: nil) (append (2 :: nil) (3 :: nil)))
  &&
 (* for all xs ys,
       length (append xs ys) = (length xs) + (length ys) *)
  (length (append (1 :: nil) (2 :: nil)) === (length (1 :: nil)) + (length (2 :: nil)))
  &&
  (length (append (1 :: 2 :: nil) (3 :: nil)) === (length (1 :: 2 :: nil)) + (length (3 :: nil)))
  &&
  (length (append (1 :: 2 :: nil) (3 :: 4 :: nil)) === (length (1 :: 2 :: nil)) + (length (3 :: 4 :: nil)))
  .

Lemma append_nil_l :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall ys : list T,
      append nil ys = ys.
Proof.
  intro t.
  intro append.
  intro s_append.
  intro ys.
  unfold specification_of_append in s_append.
  destruct s_append as [append_b _].
  rewrite -> (append_b ys).
  reflexivity.
Qed.

Lemma append_nil_r :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall xs : list T,
      append xs nil = xs.
Proof.
  intro t.
  intro append.
  intro s_append.
  intro xs.
  unfold specification_of_append in s_append.
  destruct s_append as [append_b append_i].
  induction xs as [ | x' xs' IHxs'].
    rewrite -> (append_b nil).
    reflexivity.

    rewrite -> (append_i x' xs' nil).
    rewrite -> (IHxs').
    reflexivity.
Qed.

Lemma append_assoc :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall xs ys zs : list T,
      append (append xs ys) zs = append xs (append ys zs).
Proof.
  intro t.
  intro append.
  intro s_append.
  intros xs ys zs.
  induction xs as [ | x' xs' IHxs'].
    rewrite -> (append_nil_l t append s_append ys).
    rewrite -> (append_nil_l t append s_append (append ys zs)).
    reflexivity.

    unfold specification_of_append in s_append.
    destruct s_append as [append_b append_i].
    rewrite -> (append_i x' xs' (append ys zs)).
    rewrite <- (IHxs').
    rewrite <- (append_i x' (append xs' ys) zs).
    rewrite <- (append_i x' xs' ys).
    reflexivity.
Qed.

Lemma append_length :
  forall (T : Type)
         (length : list T -> nat)
         (append : list T -> list T -> list T),
    specification_of_length T length ->
    specification_of_append T append ->
    forall xs ys : list T,
      length (append xs ys) = (length xs) + (length ys).
Proof.
  intro t.
  intro length.
  intro append.
  intro s_length.
  intro s_append.
  intros xs ys.
  induction xs as [ | x' xs' IHxs'].
     rewrite -> (append_nil_l t append s_append ys).
     unfold specification_of_length in s_length.
     destruct s_length as [length_b length_i].
     rewrite -> (length_b).
     rewrite -> (plus_0_l (length ys)).
     reflexivity.

     unfold specification_of_length in s_length.
     destruct s_length as [length_b length_i].
     rewrite -> (length_i x' xs').
     rewrite <- (one_plus_i_equals_S_i (length xs')).
     rewrite <- (plus_assoc 1 (length xs') (length ys)).
     rewrite <- (IHxs').

     unfold specification_of_append in s_append.
     destruct s_append as [append_b append_i].
     rewrite -> (append_i x' xs' ys).
     rewrite -> (length_i x' (append xs' ys)).
     rewrite -> (one_plus_i_equals_S_i (length (append xs' ys))).
     reflexivity.
Qed.  

(* Exercise: write a unit test that validates these properties. *)
(* NOTE: Did proves based on the specifications instead, I hope this is as acceptable. *) 
(* NOTE2: (5mins later); At this point I recognized, that all of these were indeed implemented below *)

Lemma nil_is_neutral_for_append_on_the_left :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall ys : list T,
      append nil ys = ys.
Proof.
  intros T append.
  unfold specification_of_append.
  intros [Hbc Hic].
  intro ys.
  apply (Hbc ys).
Qed.

Lemma nil_is_neutral_for_append_on_the_right :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall xs : list T,
      append xs nil = xs.
Proof.
  intros T append.

  unfold specification_of_append.
  intros [Hbc Hic].

  intro xs.
  induction xs as [ | x xs' IHxs'].

  apply (Hbc nil).

  rewrite -> (Hic x xs' nil).
  rewrite -> IHxs'.
  reflexivity.
Qed.

Lemma append_is_associative :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall xs ys zs: list T,
      append (append xs ys) zs = append xs (append ys zs).
Proof.
  intros T append.

  unfold specification_of_append.
  intros [Hbc Hic].

  intros xs.
  induction xs as [ | x xs' IHxs'].

  intros ys zs.
  rewrite -> (Hbc ys).
  rewrite -> (Hbc (append ys zs)).
  reflexivity.

  intros ys zs.
  rewrite -> (Hic x xs' ys).
  rewrite -> (Hic x (append xs' ys)).
  rewrite -> (Hic x xs' (append ys zs)).
  rewrite -> (IHxs' ys zs).
  reflexivity.
Qed.

(* ********** *)

Proposition append_preserves_length :
  forall (T : Type)
         (length : list T -> nat)
         (append : list T -> list T -> list T),
    specification_of_length T length ->
    specification_of_append T append ->
    forall xs ys : list T,
      length (append xs ys) = (length xs) + (length ys).
Proof.
  intros T length append.

  unfold specification_of_length.
  intros [H_length_bc H_length_ic].

  unfold specification_of_append.
  intros [H_append_bc H_append_ic].

  intro xs.
  induction xs as [ | x xs' IHxs'].

  intro ys.
  rewrite -> (H_append_bc ys).
  rewrite -> H_length_bc.
  rewrite -> plus_0_l.
  reflexivity.

  intro ys.
  rewrite -> (H_append_ic x xs' ys).
  rewrite -> (H_length_ic x (append xs' ys)).
  rewrite -> (IHxs' ys).
  rewrite -> (H_length_ic x xs').
  symmetry.
  apply plus_Sn_m.
Qed.

(* ********** *)

(* Reversing a list: *)

Definition unit_tests_for_reverse_nat (reverse : list nat -> list nat) :=
  (equal_list_nat (reverse nil)
                  nil)
  &&
  (equal_list_nat (reverse (1 :: nil))
                  (1 :: nil))
  &&
  (equal_list_nat (reverse (1 :: 2 :: nil))
                  (2 :: 1 :: nil))
  &&
  (equal_list_nat (reverse (1 :: 2 :: 3 :: nil))
                  (3 :: 2 :: 1 :: nil))
  .

Definition specification_of_reverse (T : Type) (reverse : list T -> list T) :=
  forall (append : list T -> list T -> list T),
    specification_of_append T append ->
    (reverse nil = nil)
    /\
    (forall (x : T) (xs' : list T),
       reverse (x :: xs') = append (reverse xs') (x :: nil)).

Theorem there_is_only_one_reverse :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall reverse_1 reverse_2 : list T -> list T,
      specification_of_reverse T reverse_1 ->
      specification_of_reverse T reverse_2 ->
      forall xs : list T,
        reverse_1 xs = reverse_2 xs.
Proof.
  intro i.
  intro append.
  intro s_append.
  intros reverse1 reverse2.
  intros s_reverse1 s_reverse2.
  unfold specification_of_reverse in s_reverse1.
  unfold specification_of_reverse in s_reverse2.
  destruct (s_reverse1 append s_append) as [reverse1_b reverse1_i].
  destruct (s_reverse2 append s_append) as [reverse2_b reverse2_i].
  clear s_reverse1.
  clear s_reverse2.
  intro xs.
  induction xs as [ | x' xs' IHxs'].
    rewrite -> (reverse1_b).
    rewrite -> (reverse2_b).
    reflexivity.

    rewrite -> (reverse1_i x' xs').
    rewrite -> (IHxs').
    rewrite <- (reverse2_i x' xs').
    reflexivity.
Qed.

(* ***** *)

(* An implementation of reverse: *)

Fixpoint reverse_ds (T : Type) (xs : list T) : list T :=
  match xs with
    | nil => nil
    | x :: xs' => append_v1 T (reverse_ds T xs') (x :: nil)
  end.

Definition reverse_v1 (T : Type) (xs : list T) : list T :=
  reverse_ds T xs.

Compute unit_tests_for_reverse_nat (reverse_v1 nat).

Lemma unfold_reverse_ds_base_case :
  forall (T : Type),
    reverse_ds T nil = nil.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic reverse_ds.
Qed.

Lemma unfold_reverse_ds_induction_case :
  forall (T : Type)
         (x : T)
         (xs' : list T),
    reverse_ds T (x :: xs') =
    append_v1 T (reverse_ds T xs') (x :: nil).
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic reverse_ds.
Qed.

Lemma fold_append_ds :
  forall (T : Type)
         (xs : list T)
         (ys : list T),
    append_v1 T xs ys = append_ds T xs ys.
Proof.
  intro t.
  intros xs ys.
  unfold append_v1.
  reflexivity.
Qed.
  
Proposition reverse_v1_fits_the_specification_of_reverse :
  forall T : Type,
    specification_of_reverse T (reverse_v1 T).
Proof.
  intro T.
  unfold specification_of_reverse.
  intro append.
  intro s_append.
  split.
    unfold reverse_v1.
    rewrite -> (unfold_reverse_ds_base_case T).
    reflexivity.

    intros x xs'.
    unfold reverse_v1.
    rewrite -> (there_is_only_one_append T append (append_ds T) s_append (append_v1_fits_the_specification_of_append T) (reverse_ds T xs') (x :: nil)).
    rewrite -> (unfold_reverse_ds_induction_case T x xs').
    unfold append_v1.
    reflexivity.
Qed.

(* ***** *)

(* A second implementation of reverse, with an accumulator : *)

Fixpoint reverse_acc (T : Type) (xs a : list T) : list T :=
  match xs with
    | nil => a
    | x :: xs' => reverse_acc T xs' (x :: a)
  end.

Definition reverse_v2 (T : Type) (xs : list T) :=
  reverse_acc T xs nil.

Compute unit_tests_for_reverse_nat (reverse_v2 nat).

Lemma reverse_acc_bc :
  forall (T : Type)
         (a : list T),
    reverse_acc T nil a = a.
Proof.
  unfold_tactic reverse_acc.
Qed.

Lemma reverse_acc_ic :
  forall (T : Type)
         (x : T)
         (xs : list T)
         (a : list T),
    reverse_acc T (x :: xs) a = reverse_acc T xs (x :: a).
Proof.
  unfold_tactic reverse_acc.
Qed.

(* A useful lemma (Eureka): *)

Lemma about_reverse_helper :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall x : T,
      forall xs : list T,
        append (x :: nil) xs = (x :: xs).
Proof.
  intro T.
  intro append.
  intro s_append.
  intro x.
  intro xs.
  
  unfold specification_of_append in s_append.
  destruct s_append as [append_b append_i].
  rewrite -> (append_i x nil xs).
  rewrite -> (append_b xs).
  reflexivity.
Qed.

Lemma about_reverse_acc :
  forall (T : Type)
         (append : list T -> list T -> list T),
    specification_of_append T append ->
    forall xs a : list T,
      reverse_acc T xs a = append (reverse_acc T xs nil) a.
Proof.
  intro T.
  intro append.
  intro s_append.
  intro xs.
  
  induction xs as [ | xs' xxs' IHxxs'].
    intro a.
    rewrite -> (reverse_acc_bc T a). 
    rewrite -> (reverse_acc_bc T nil).
    unfold specification_of_append in s_append.
    destruct s_append as [append_b _].
    rewrite -> (append_b a).
    reflexivity.

    intro a.
    rewrite -> (reverse_acc_ic T xs' xxs' a).
    rewrite -> (IHxxs' (xs' :: a)).
    rewrite -> (reverse_acc_ic T xs' xxs' nil).
    rewrite -> (IHxxs' (xs' :: nil)).
    rewrite -> (append_is_associative T append s_append (reverse_acc T xxs' nil) (xs' :: nil) a).
    rewrite -> (about_reverse_helper T append s_append xs' a).
    reflexivity.
Qed.

Proposition reverse_v2_fits_the_specification_of_reverse :
  forall T : Type,
    specification_of_reverse T (reverse_v2 T).
Proof.
  intro T.
  intro append.
  intro s_append.
  split.
    unfold reverse_v2.
    rewrite -> (reverse_acc_bc T nil).
    reflexivity.

    intro x.
    intro xs'.
    unfold reverse_v2.
    rewrite -> (reverse_acc_ic T x xs' nil).
    rewrite -> (about_reverse_acc T append s_append xs' (x :: nil)).
    reflexivity.
Qed.

(* ********** *)

(* Properties:
     for all xs,
       length xs = length (reverse xs)
     forall xs ys,
       reverse (append xs ys) = append (reverse ys) (reverse xs)
     forall xs,
       reverse (reverse xs) = xs
*)
Definition unit_tests_for_reverse_nat_2 (length : list nat -> nat) (append : list nat -> list nat -> list nat) (reverse : list nat -> list nat) :=
(*  forall xs,
       length xs = length (reverse xs) *)
  (length nil === length (reverse nil))
  &&
  (length (1 :: nil) === length (reverse (1 :: nil)))
  &&
  (length (1 :: 2 :: nil) === length (reverse (1 :: 2 :: nil)))
  &&
(* forall xs ys,
       reverse (append xs ys) = append (reverse ys) (reverse xs) *)
  (equal_list_nat (reverse (append nil nil))
                  (append (reverse nil) (reverse nil)))
  &&
  (equal_list_nat (reverse (append (1 :: nil) nil))
                  (append (reverse nil) (reverse (1 :: nil))))
  &&
  (equal_list_nat (reverse (append (1 :: nil) (2 :: nil)))
                  (append (reverse (2 :: nil)) (reverse (1 :: nil))))
  &&
  (equal_list_nat (reverse (append (1 :: 2 :: nil) nil))
                  (append (reverse nil) (reverse (1 :: 2 :: nil))))
  &&
  (equal_list_nat (reverse (append (1 :: 2 :: nil) (3 :: nil)))
                  (append (reverse (3 :: nil)) (reverse (1 :: 2 :: nil))))
  &&
  (equal_list_nat (reverse (append (1 :: 2 :: nil) (3 :: 4 :: nil)))
                  (append (reverse (3 :: 4 :: nil)) (reverse (1 :: 2 :: nil))))
  &&
  (* forall xs,
       reverse (reverse xs) = xs *)
  (equal_list_nat (reverse (reverse nil))
                  (nil))
  &&
  (equal_list_nat (reverse (reverse (1 :: nil)))
                  (1 :: nil))
  &&
  (equal_list_nat (reverse (reverse (1 :: 2 :: nil)))
                  (1 :: 2 :: nil))
  &&
  (equal_list_nat (reverse (reverse (1 :: 2 :: 3 :: nil)))
                  (1 :: 2 :: 3 ::nil))
  .

(* Exercise: write a unit test that validates these properties. *)

Proposition reverse_preserves_length :
  forall (T : Type)
         (length : list T -> nat)
         (append : list T -> list T -> list T)
         (reverse : list T -> list T),
    specification_of_length T length ->
    specification_of_append T append ->
    specification_of_reverse T reverse ->
    forall xs : list T,
      length xs = length (reverse xs).
Proof.
  intro T.
  intro length.
  intro append.
  intro reverse.
  intro s_length.
  intro s_append.
  intro s_reverse.
  intro xs.
  induction xs as [ | xs' xss' IHxss'].
    unfold specification_of_length in s_length. 
    destruct s_length as [length_b _].
    rewrite -> (length_b).
    unfold specification_of_reverse in s_reverse.
    destruct (s_reverse append s_append) as [reverse_b _].
    rewrite -> (reverse_b).
    rewrite -> (length_b).
    reflexivity.

    assert (s_length' := s_length).
    unfold specification_of_length in s_length.
    destruct s_length as [length_b length_i].
    rewrite -> (length_i xs' xss').
    rewrite -> (IHxss').
    rewrite <- (length_i xs' (reverse xss')).
    unfold specification_of_reverse in s_reverse.
    destruct (s_reverse append s_append) as [_ reverse_i].
    rewrite -> (reverse_i xs' xss').
    rewrite -> (length_i xs' (reverse xss')).
    rewrite -> (append_length T length append s_length' s_append (reverse xss') (xs' :: nil)).
    rewrite -> (length_i xs' nil).
    rewrite -> (length_b).
    rewrite <- (one_plus_i_equals_S_i (length (reverse xss'))).
    rewrite -> (plus_comm 1 (length (reverse xss'))).
    reflexivity.
Qed.

Proposition reverse_preserves_append_sort_of :
  forall (T : Type)
         (append : list T -> list T -> list T)
         (reverse : list T -> list T),
    specification_of_append T append ->
    specification_of_reverse T reverse ->
    forall xs ys : list T,
      reverse (append xs ys) = append (reverse ys) (reverse xs).
Proof.
  intro T.
  intro append.
  intro reverse.
  intro s_append.
  intro s_reverse.
  intro xs.
  induction xs as [ | xs' xss' IHxss'].
    intro ys.
    rewrite -> (append_nil_l T append s_append ys).
    unfold specification_of_reverse in s_reverse.
    destruct (s_reverse append s_append) as [reverse_b _].
    rewrite -> (reverse_b).
    rewrite -> (append_nil_r T append s_append (reverse ys)).
    reflexivity.

    assert (s_append' := s_append).
    intro ys.
    unfold specification_of_append in s_append.
    destruct s_append as [append_b append_i].
    rewrite -> (append_i xs' xss' ys).
    unfold specification_of_reverse in s_reverse.
    destruct (s_reverse append s_append') as [reverse_b reverse_i].
    clear s_reverse.
    rewrite -> (reverse_i xs' (append xss' ys)).
    rewrite -> (IHxss' ys).
    rewrite -> (append_is_associative T append s_append' (reverse ys) (reverse xss') (xs'  :: nil)).
    rewrite <- (reverse_i xs' xss').
    reflexivity.
Qed.

Proposition reverse_is_involutive :
  forall (T : Type)
         (append : list T -> list T -> list T)
         (reverse : list T -> list T),
    specification_of_append T append ->
    specification_of_reverse T reverse ->
    forall xs : list T,
      reverse (reverse xs) = xs.
Proof.
  intro T.
  intro append.
  intro reverse.
  intro s_append.
  intro s_reverse.
  intro xs.
  induction xs as [ | xs' xss' IHxss'].
    unfold specification_of_reverse in s_reverse.
    destruct (s_reverse append s_append) as [reverse_b _].
    rewrite -> (reverse_b).
    rewrite -> (reverse_b).
    reflexivity.

    assert (s_reverse' := s_reverse).
    unfold specification_of_reverse in s_reverse.
    destruct (s_reverse append s_append) as [reverse_b reverse_i].
    rewrite -> (reverse_i xs' xss').

    Check reverse_preserves_append_sort_of.
    rewrite -> (reverse_preserves_append_sort_of T append reverse s_append s_reverse' (reverse xss') (xs' :: nil)).
    rewrite -> (IHxss').
    rewrite -> (reverse_i xs' nil).
    rewrite -> (reverse_b).
    rewrite -> (append_nil_l T append s_append (xs' :: nil)).
    rewrite -> (about_reverse_helper T append s_append xs' xss').
    reflexivity.
Qed.

(* ********** *)

(* Mapping a function over the elements of a list: *)

Definition unit_tests_for_map_nat (map : (nat -> nat) -> list nat -> list nat) :=
  (equal_list_nat (map (fun n => n)
                       nil)
                  nil)
  &&
  (equal_list_nat (map (fun n => n)
                       (1 :: nil))
                  (1 :: nil))
  &&
  (equal_list_nat (map (fun n => n)
                       (1 :: 2 :: 3 :: nil))
                  (1 :: 2 :: 3 :: nil))
  &&
  (equal_list_nat (map (fun n => S n)
                       nil)
                  nil)
  &&
  (equal_list_nat (map (fun n => S n)
                       (1 :: nil))
                  (2 :: nil))
  &&
  (equal_list_nat (map (fun n => S n)
                       (1 :: 2 :: 3 :: nil))
                  (2 :: 3 :: 4 :: nil))
  &&
  (equal_list_nat (map (fun n => S (S n))
                       nil)
                  nil)
  &&
  (equal_list_nat (map (fun n => S (S n))
                       (1 :: nil))
                  (3 :: nil))
  &&
  (equal_list_nat (map (fun n => S (S n))
                       (1 :: 2 :: 3 :: nil))
                  (3 :: 4 :: 5 :: nil))
  &&
  (equal_list_nat (map (fun n => 2 * n)
                       nil)
                  nil)
  &&
  (equal_list_nat (map (fun n => 2 * n)
                       (1 :: nil))
                  (2 :: nil))
  &&
  (equal_list_nat (map (fun n => 2 * n)
                       (1 :: 2 :: 3 :: nil))
                  (2 :: 4 :: 6 :: nil))
  .

Definition specification_of_map (T1 T2 : Type) (map : (T1 -> T2) -> list T1 -> list T2) :=
  (forall f : T1 -> T2,
     map f nil = nil)
  /\
  (forall (f : T1 -> T2) (x : T1) (xs' : list T1),
     map f (x :: xs') = (f x) :: (map f xs')).

Theorem there_is_only_one_map :
  forall (T1 T2 : Type)
         (map_1 map_2 : (T1 -> T2) -> list T1 -> list T2),
    specification_of_map T1 T2 map_1 ->
    specification_of_map T1 T2 map_2 ->
    forall (f : T1 -> T2)
           (xs : list T1),
      map_1 f xs = map_2 f xs.
Proof.
  intros T1 T2.
  intros map1 map2.
  intros s_map1 s_map2.
  intro f.
  intro xs.
  induction xs as [ | xs' xss' IHxss'].
    unfold specification_of_map in s_map1.
    unfold specification_of_map in s_map2.
    destruct s_map1 as [map1_b _].
    destruct s_map2 as [map2_b _].
    rewrite -> (map1_b f).
    rewrite -> (map2_b f).
    reflexivity.

    unfold specification_of_map in s_map1.
    unfold specification_of_map in s_map2.
    destruct s_map1 as [_ map1_h].
    destruct s_map2 as [_ map2_h].
    rewrite -> (map1_h f xs' xss').
    rewrite -> (IHxss').
    rewrite <- (map2_h f xs' xss').
    reflexivity.
Qed.

(* ***** *)

(* An implementation of map: *)

Fixpoint map_ds (T1 T2 : Type) (f : T1 -> T2) (xs : list T1) : list T2 :=
  match xs with
    | nil => nil
    | x :: xs' => (f x) :: (map_ds T1 T2 f xs')
  end.

Definition map_v1 (T1 T2 : Type) (f : T1 -> T2) (xs : list T1) : list T2 :=
  map_ds T1 T2 f xs.

Compute unit_tests_for_map_nat (map_v1 nat nat).

Lemma map_ds_bc :
  forall (T1 T2 : Type)
         (f : T1 -> T2),
    map_ds T1 T2 f nil = nil.
Proof.
  unfold_tactic map_ds.
Qed.

Lemma map_ds_ic :
  forall (T1 T2 : Type)
         (f : T1 -> T2)
         (x : T1)
         (xs' : list T1),
    map_ds T1 T2 f (x :: xs') = (f x) :: (map_ds T1 T2 f xs').
Proof.
  unfold_tactic map_ds.
Qed.

Proposition map_v1_fits_the_specification_of_map :
  forall T1 T2 : Type,
    specification_of_map T1 T2 (map_v1 T1 T2).
Proof.
  intros T1 T2.
  split.
    intro f.
    unfold map_v1.
    rewrite -> (map_ds_bc T1 T2 f).
    reflexivity.

    intro f.
    intro x.
    intro xs'.
    unfold map_v1.
    rewrite -> (map_ds_ic T1 T2 f x xs').
    reflexivity.
Qed.

(* ********** *)

(* Properties:
     for all f1 f2 xs,
       map f2 (map f1 xs) = map (fun x => f2 (f1 x)) xs
     for all f xs ys,
        map f (append xs ys) = append (map f xs) (map f ys)
     for all f xs,
       map f (reverse xs) = reverse (map f xs)
*)

(* Exercise: write a unit test that validates these properties. *)
(* TODO: Write these *)

Proposition listlessness_of_map :
  forall (T1 T2 T3 : Type)
         (map12 : (T1 -> T2) -> list T1 -> list T2)
         (map23 : (T2 -> T3) -> list T2 -> list T3)
         (map13 : (T1 -> T3) -> list T1 -> list T3),
    specification_of_map T1 T2 map12 ->
    specification_of_map T2 T3 map23 ->
    specification_of_map T1 T3 map13 ->
    forall (f1 : T1 -> T2)
           (f2 : T2 -> T3)
           (xs : list T1),
      map23 f2 (map12 f1 xs) = map13 (fun x => f2 (f1 x)) xs.
Proof.
  intros T1 T2 T3.
  intros map12 map23 map13.
  intros s_map12 s_map23 s_map13.
  intros f1 f2.
  intro xs.
  induction xs as [ | xs' xss' IHxss'].
    unfold specification_of_map in s_map12.
    destruct s_map12 as [map12_b _].
    rewrite -> (map12_b f1).
    unfold specification_of_map in s_map23.
    destruct s_map23 as [map23_b _].
    rewrite -> (map23_b f2).
    unfold specification_of_map in s_map13.
    destruct s_map13 as [map13_b _].
    rewrite -> (map13_b (fun x : T1 => f2 (f1 x))).
    reflexivity.

    unfold specification_of_map in s_map12.
    destruct s_map12 as [map12_b map12_i].
    rewrite -> (map12_i f1 xs' xss').
    unfold specification_of_map in s_map23.
    destruct s_map23 as [map23_b map23_i].
    rewrite -> (map23_i f2 (f1 xs') (map12 f1 xss')).
    rewrite -> (IHxss').

    unfold specification_of_map in s_map13.
    destruct s_map13 as [map13_b map13_i].
    rewrite -> (map13_i (fun x : T1 => f2 (f1 x)) xs' xss').
    reflexivity.
Qed.

Proposition append_preserves_map :
  forall (T1 T2 : Type)
         (map : (T1 -> T2) -> list T1 -> list T2)
         (append_1 : list T1 -> list T1 -> list T1)
         (append_2 : list T2 -> list T2 -> list T2),
    specification_of_map T1 T2 map ->
    specification_of_append T1 append_1 ->
    specification_of_append T2 append_2 ->
    forall (f : T1 -> T2) (xs ys : list T1),
       map f (append_1 xs ys) = append_2 (map f xs) (map f ys).
Proof.
  intros T1 T2.
  intro map.
  intros append_1 append_2.
  intro s_map.
  intros s_append1 s_append2.
  intro f.
  intro xs.
  induction xs as [ | xs' xss' IHxss'].
    intro ys.
    unfold specification_of_append in s_append1.
    unfold specification_of_append in s_append2.
    destruct s_append1 as [append1_b _].
    destruct s_append2 as [append2_b _].
    rewrite -> (append1_b ys).

    unfold specification_of_map in s_map.
    destruct s_map as [map_b _].
    rewrite -> (map_b f).
    rewrite -> (append2_b (map f ys)).
    reflexivity.

    intro ys.
    unfold specification_of_append in s_append1.
    unfold specification_of_append in s_append2.
    destruct s_append1 as [append1_b append1_h].
    destruct s_append2 as [append2_b append2_h].
    rewrite -> (append1_h xs' xss' ys).
    unfold specification_of_map in s_map.
    destruct s_map as [map_b map_h].
    rewrite -> (map_h f xs' xss').
    rewrite -> (append2_h (f xs') (map f xss')).
    rewrite <- (IHxss' ys).
    rewrite -> (map_h f xs' (append_1 xss' ys)).
    reflexivity.
Qed.

Proposition reverse_preserves_map_sort_of :
  forall (T1 T2 : Type)
         (append_1 : list T1 -> list T1 -> list T1)
         (append_2 : list T2 -> list T2 -> list T2)
         (reverse_1 : list T1 -> list T1)
         (reverse_2 : list T2 -> list T2)
         (map : (T1 -> T2) -> list T1 -> list T2),
    specification_of_append T1 append_1 ->
    specification_of_append T2 append_2 ->
    specification_of_reverse T1 reverse_1 ->
    specification_of_reverse T2 reverse_2 ->
    specification_of_map T1 T2 map ->
    forall (f : T1 -> T2)
           (xs : list T1),
      map f (reverse_1 xs) = reverse_2 (map f xs).
Proof.
  intros T1 T2.
  intros append1 append2.
  intros reverse1 reverse2.
  intro map.
  intros s_append1 s_append2.
  intros s_reverse1 s_reverse2.
  intro s_map.
  intro f.
  intro xs.
  induction xs as [ | xs' xss' IHxss'].
    unfold specification_of_reverse in s_reverse1.
    destruct (s_reverse1 append1 s_append1) as [reverse1_b _].
    rewrite -> (reverse1_b).
    unfold specification_of_map in s_map.
    destruct s_map as [map_b _].
    rewrite -> (map_b f).
    unfold specification_of_reverse in s_reverse2.
    destruct (s_reverse2 append2 s_append2) as [reverse2_b _].
    rewrite -> (reverse2_b).
    reflexivity.

    assert (s_map' := s_map).
    unfold specification_of_map in s_map.
    destruct s_map as [map1_b map1_i].
    rewrite -> (map1_i f xs' xss').

    unfold specification_of_reverse in s_reverse2.
    destruct (s_reverse2 append2 s_append2) as [reverse2_b reverse2_i].
    rewrite -> (reverse2_i (f xs') (map f xss')).
    rewrite <- (IHxss').
    
    clear reverse2_i.
    clear reverse2_b.
    clear IHxss'.
    clear s_reverse2.
    clear reverse2.

    unfold specification_of_reverse in s_reverse1.
    destruct (s_reverse1 append1 s_append1) as [reverse1_b reverse1_i].
    rewrite -> (reverse1_i xs' xss').

    Check append_preserves_map.
    rewrite -> (append_preserves_map T1 T2 map append1 append2 s_map' s_append1 s_append2 f (reverse1 xss') (xs' :: nil)).

    rewrite -> (map1_i f xs' nil).
    rewrite -> (map1_b f).
    reflexivity.
Qed.

(* ********** *)

(* end of week_37b_lists.v *)
