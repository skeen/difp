(* week_35_exercises.v *)
(* dIFP 2014-2015, Q1 *)
(* Olivier Danvy <danvy@cs.au.dk> *)

(* Handin by Emil 'Skeen' Madsen 20105376 *)

(* Exercise 1:
   Prove that conjunction and disjunctions are associative.
*)

Lemma conjunction_is_associative_from_left_to_right :
  forall P1 P2 P3 : Prop,
    (P1 /\ P2) /\ P3 -> P1 /\ (P2 /\ P3).
Proof.
  intros P1 P2 P3.
  intros [[H_P1 H_P2] H_P3].
  split.
    apply H_P1.
    split.
      apply H_P2.
      apply H_P3.
Qed.

Theorem conjunction_is_associative_either_way :
  forall P1 P2 P3 : Prop,
    P1 /\ (P2 /\ P3) <-> (P1 /\ P2) /\ P3.
Proof.
  split.
    (* Right to left *)
    intros [H_P1 [H_P2 H_P3]].
    split.
      split.
        apply H_P1.
        apply H_P2.
      apply H_P3.
  (* Left to right *)
  apply (conjunction_is_associative_from_left_to_right P1 P2 P3).
Qed.

Lemma disjunction_is_associative_from_left_to_right :
  forall P1 P2 P3 : Prop,
    (P1 \/ P2) \/ P3 -> P1 \/ (P2 \/ P3).
Proof.
  intros P1 P2 P3.
  intros [[H_P1 | H_P2] | H_P3].
  left. apply H_P1.
  right. left. apply H_P2.
  right. right. apply H_P3.
Qed.

Lemma disjunction_is_associative_from_right_to_left :
  forall P1 P2 P3 : Prop,
    P1 \/ (P2 \/ P3) -> (P1 \/ P2) \/ P3.
Proof.
  intros P1 P2 P3.
  intros [H_P1 | [H_P2 | H_P3]].
  left. left. apply H_P1.
  left. right. apply H_P2.
  right. apply H_P3.
Qed.

Theorem disjunction_is_associative_either_way :
  forall P1 P2 P3 : Prop,
    P1 \/ (P2 \/ P3) <-> (P1 \/ P2) \/ P3.
Proof.
  split.
    apply (disjunction_is_associative_from_right_to_left P1 P2 P3).
    apply (disjunction_is_associative_from_left_to_right P1 P2 P3).
Qed.

(* Exercise 2:
   Prove that conjunction and disjunctions are commutative.
*)

Lemma conjunction_is_commutative :
  forall P Q : Prop,
    P /\ Q -> Q /\ P.
Proof.
  intros P Q.
  intros [H_P H_Q].
  split.
    apply H_Q.
    apply H_P.
Qed.

Lemma conjunction_is_commutative_either_way :
  forall P Q : Prop,
    P /\ Q <-> Q /\ P.
Proof.
  intros P Q.
  split.
  apply (conjunction_is_commutative P Q).
  apply (conjunction_is_commutative Q P).
Qed.

Lemma disjunction_is_commutative :
  forall P Q : Prop,
    P \/ Q -> Q \/ P.
Proof.
  intros P Q.
  intros [H_P | H_Q].
  right. apply H_P.
  left. apply H_Q.
Qed.

Lemma disjunction_is_commutative_either_way :
  forall P Q : Prop,
    P \/ Q <-> Q \/ P.
Proof.
  split.
  apply (disjunction_is_commutative P Q).
  apply (disjunction_is_commutative Q P).
Qed.

(* Exercise 3:
   Prove the following lemma, Curry_and_unCurry.
*)

Lemma Curry :
  forall P Q R : Prop,
    (P /\ Q -> R) -> (P -> Q -> R).
Proof.
  intros A B C.
  intro H_A_and_B_implies_C.
  intro H_A.
  intro H_B.
  apply H_A_and_B_implies_C.
  split.
    apply H_A.
    apply H_B.
Qed.

Lemma unCurry :
  forall P Q R : Prop,
    (P -> Q -> R) -> (P /\ Q -> R).
Proof.
  intros A B C.
  intros H_A_implies_B_implies_C.
  intros [H_A H_B].
  apply H_A_implies_B_implies_C.
  apply H_A.
  apply H_B.
Qed.

Lemma Curry_and_unCurry :
  forall P Q R : Prop,
    (P /\ Q -> R) <-> P -> Q -> R.
Proof.
  intros A B C.
  split.
    apply (Curry A B C).
    apply (unCurry A B C).
Qed.

(* Here is how to import a Coq library about arithmetic expressions: *)

Require Import Arith.

Check plus_comm.

(*
plus_comm
     : forall n m : nat, n + m = m + n
*)

Lemma comm_a :
  forall a b c : nat,
    (a + b) + c = c + (b + a).
Proof.
  intros a b c.
  rewrite -> (plus_comm a b).
  rewrite -> (plus_comm (b + a) c).
  reflexivity.

  Restart.

  intros a b c.
  rewrite -> (plus_comm (a + b) c).
  rewrite -> (plus_comm a b).
  reflexivity.

  Restart.

  intros a b c.
  rewrite -> (plus_comm c (b + a)).
  rewrite -> (plus_comm b a).
  reflexivity.

  Restart.

  intros a b c.
  rewrite <- (plus_comm (b + a) c).
  rewrite <- (plus_comm a b).
  reflexivity.

  Restart.

  intros a b c.
  rewrite <- (plus_comm a b).
  rewrite <- (plus_comm (a + b) c).
  reflexivity.

  Restart.

  intros a b c.
  rewrite -> (plus_comm a b).
  rewrite <- (plus_comm (b + a) c).
  reflexivity.
Qed.

(* Exercise 4:
   Add a couple more proofs for Lemma comm_a.
*)

Lemma comm_b :
  forall x y z : nat,
    (x + y) + z = z + (y + x).
Proof.
  intros x y z.
  apply (comm_a x y z).

  Restart.

  apply comm_a.
Qed.

(* symmetry *)

Lemma comm_c :
  forall a b c : nat,
    c + (b + a) = (a + b) + c.
Proof.
  intros a b c.
  symmetry.
  apply (comm_a a b c).
Qed.

(* ********** *)

Check plus_assoc.

(*
plus_assoc
     : forall n m p : nat, n + (m + p) = n + m + p
*)

(* Exercise 5, for the over-achievers *)

Lemma assoc_a :
  forall a b c d : nat,
    a + (b + (c + d)) = ((a + b) + c) + d.
Proof.
  intros a b c d.
  rewrite -> (plus_assoc a b (c + d)).
  rewrite <- (plus_assoc (a + b) c d).
  reflexivity.

  Restart.
  intros a b c d.
  rewrite (plus_assoc b c d).
  rewrite (plus_assoc a (b+c) d).
  rewrite (plus_assoc a b c).
  reflexivity.
Qed.

(* Exercise 6:
   Prove the following lemma, mixed_a.
*)

Lemma mixed_a :
forall a b c d : nat,
    (c + (a + b)) + d = (b + (d + c)) + a.
Proof.
  intros a b c d.
  rewrite (plus_assoc b d c).
  rewrite (plus_assoc c a b).
  rewrite (plus_comm (c+a) b).
  rewrite (plus_assoc b c a).
  rewrite (plus_comm (b+c+a) d).
  rewrite (plus_assoc d (b+c) a).
  rewrite (plus_assoc d b c).
  rewrite (plus_comm d b).
  reflexivity.
Qed.

(* end of week_35_exercises.v *)
