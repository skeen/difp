(* week_38c_mystery_functions_after_todays_lecture.v *)
(* dIFP 2014-2015, Q1, Week 38 *)
(* Olivier Danvy <danvy@cs.au.dk> *)

(* Handin by;
 * Emil 'Skeen' Madsen - 20105376
 * Søren Krogh         - 20105661
 *)
(* Sorry for the late handin! *)

(* ********** *)

Require Import Arith Bool unfold_tactic.

Notation "A =n= B" := (beq_nat A B) (at level 70, right associativity).

Notation "A =b= B" := (eqb A B) (at level 70, right associativity).

(* ********** *)

Lemma nat_ind2 :
  forall P : nat -> Prop,
    P 0 ->
    P 1 ->
    (forall i : nat,
      P i -> P (S i) -> P (S (S i))) ->
    forall n : nat,
      P n.
Proof.
  intros P H_P0 H_P1 H_PSS n.
  assert (consecutive :
    forall x : nat,
      P x /\ P (S x)).
    intro x.
    induction x as [ | x' [IHx' IHSx']].

    split.
      exact H_P0.
    exact H_P1.

    split.
      exact IHSx'.
    exact (H_PSS x' IHx' IHSx').

  destruct (consecutive n) as [ly _].
  exact ly.
Qed.

Lemma S_equals_plus_1 :
  forall x : nat,
    S x = 1 + x.
Proof.
  intro x.
  ring.
Qed.

(* Are the following specifications unique?
   What are then the corresponding functions?

   Spend the rest of your dIFP weekly time
   to answer these questions
   for the specifications below.
   (At least 7 specifications would be nice.)
*)

(* ********** *)

Definition specification_of_the_mystery_function_0 (f : nat -> nat) :=
  (f 0 = 1)
  /\
  (forall i j : nat,
    f (S (i + j)) = f i + f j).

(* ***** *)

Proposition there_is_only_one_mystery_function_0 :
  forall f g : nat -> nat,
    specification_of_the_mystery_function_0 f ->
    specification_of_the_mystery_function_0 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_0.
  intros [f_bc f_ic [g_bc g_ic]].
  intro n.
  induction n as [ | n' IHn'].  
    rewrite -> (f_bc).
    rewrite -> (g_bc).
    reflexivity.

    rewrite <- (plus_0_r n').
    rewrite -> (f_ic).
    rewrite -> (g_ic).
    rewrite -> (IHn').
    rewrite -> (f_bc).
    rewrite -> (g_bc).
    reflexivity.
Qed.

(* ***** *)

(* Flesh out the following unit test
   as you tabulate mystery_function_0:
*)
Definition unit_test_for_the_mystery_function_0 (f : nat -> nat) :=
(*
   The following equality is in the specification:
*)
  (f 0 =n= 1)
(*
   f 1
   = (* because S (i + j) = 1 if i = 0 and j = 0 *)
   f (S (0 + 0))    
   = (* by definition of mystery_function_0 *)
   f 0 + f 0
   = (* by definition of mystery_function_0 *)
   1 + 1
   =
   2
*)
  &&
  (f 1 =n= 2)
(*
   Similarly,
   f 2
   =
   f (S (0 + 1))
   =
   f 0 + f 1
   =
   1 + 2
   =
   3
*)
  &&
  (f 2 =n= 3)
(*
   We therefore conjecture that the mystery function
   is the successor function,
   i.e., the function that adds 1 to its argument.
*)
  .

Compute unit_test_for_the_mystery_function_0 S.

(* ***** *)

Theorem and_the_mystery_function_0_is_dot_dot_dot :
  specification_of_the_mystery_function_0 S.
Proof.
  unfold specification_of_the_mystery_function_0.
  split.
    reflexivity.

    intros i j.
    ring.
Qed.  

(* ********** *)

Definition specification_of_the_mystery_function_1 (f : nat -> nat) :=
  (f 0 = 0)
  /\
  (forall i j : nat,
    f (i + S j) = f i + S (f j)).

(* ***** *)

Proposition there_is_only_one_mystery_function_1 :
  forall f g : nat -> nat,
    specification_of_the_mystery_function_1 f ->
    specification_of_the_mystery_function_1 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_1.
  intros [f_bc f_ic [g_bc g_ic]].
  intro n.
  induction n as [ | n' IHn'].
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.

    rewrite <- (plus_0_l (S n')).
    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> IHn'.
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.
Qed.

(* ***** *)

(* Flesh out the following unit test
   as you tabulate mystery_function_1:
*)
Definition unit_test_for_the_mystery_function_1 (f : nat -> nat) :=
(*
   The following equality is in the specification:
*)
  (f 0 =n= 0)
(*
   f 1
   = ...
*)
  &&
  (f 1 =n= 1)
  &&
  (f 2 =n= 2)
  &&
  (f 3 =n= 3)
  .

Compute unit_test_for_the_mystery_function_1 id.

(* ***** *)

Theorem and_the_mystery_function_1_is_dot_dot_dot :
  specification_of_the_mystery_function_1 id.
Proof.
  unfold specification_of_the_mystery_function_1.
  split.
    unfold id.
    reflexivity.

    intros i j.
    unfold id.
    reflexivity.
Qed.  

(* ********** *)

Definition specification_of_the_mystery_function_2 (f : nat -> nat) :=
  (f 0 = 0)
  /\
  (forall i j : nat,
    f (S (i + j)) = S (f i) + S (f j)).

Proposition there_is_only_one_mystery_function_2 :
  forall f g : nat -> nat,
    specification_of_the_mystery_function_2 f ->
    specification_of_the_mystery_function_2 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_2.
  intros [f_bc f_ic [g_bc g_ic]].
  intro n.
  induction n as [ | n' IHn'].
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.

    rewrite <- (plus_0_l n').
    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> IHn'.
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.
Qed.

Definition unit_test_for_the_mystery_function_2 (f : nat -> nat) :=
  (f 0 =n= 0)
  &&
  (f 1 =n= 2)
  &&
  (f 2 =n= 4)
  &&
  (f 3 =n= 6)
  &&
  (f 4 =n= 8)
  .

Compute unit_test_for_the_mystery_function_2 (mult 2).

Theorem and_the_mystery_function_2_is_dot_dot_dot :
  specification_of_the_mystery_function_2 (mult 2).
Proof.
  unfold specification_of_the_mystery_function_2.
  split.
    rewrite mult_0_r.
    reflexivity.

    intros i j.
    ring.
Qed. 

(* ********** *)

Definition specification_of_the_mystery_function_3 (f : nat -> nat) :=
  (f 0 = 1)
  /\
  (forall i j : nat,
    f (S (i + j)) = S (f i) + S (f j)).

Proposition there_is_only_one_mystery_function_3 :
  forall f g : nat -> nat,
    specification_of_the_mystery_function_3 f ->
    specification_of_the_mystery_function_3 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_3.
  intros [f_bc f_ic [g_bc g_ic]].
  intro n.
  induction n as [ | n' IHn'].
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.

    rewrite <- (plus_0_l n').
    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> IHn'.
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.
Qed.

Definition unit_test_for_the_mystery_function_3 (f : nat -> nat) :=
  (f 0 =n= 1)
  &&
  (f 1 =n= 4)
  &&
  (f 2 =n= 7)
  &&
  (f 3 =n= 10)
  .

Definition mult3_plus_1 (x : nat) : nat :=
  (plus (mult x 3) 1).  

Compute unit_test_for_the_mystery_function_3 mult3_plus_1.

Theorem and_the_mystery_function_3_is_dot_dot_dot :
  specification_of_the_mystery_function_3 mult3_plus_1.
Proof.
  unfold specification_of_the_mystery_function_3.
  split.
    unfold mult3_plus_1.
    ring.

    intros i j.
    unfold mult3_plus_1.
    ring.
Qed. 

(* ********** *)

Definition specification_of_the_mystery_function_4 (f : nat -> nat) :=
  (f 0 = 0)
  /\
  (forall i j : nat,
    f (i + j) = f i + f j).
(* Note: Specification found not to be unique, proof removed because it ended with an Abort. *)

Fixpoint myst4v1 (x : nat) : nat :=
  match x with
    | 0 => 0
    | (S x) => myst4v1 x
  end.

Lemma myst4v1_bc1 :
  myst4v1 0 = 0.
Proof.
  unfold_tactic myst4v1.
Qed.

Lemma myst4v1_ic :
  forall x : nat,
    myst4v1 (S x) = myst4v1 x.
Proof.
  unfold_tactic myst4v1.
Qed.

Lemma S_outside_good :
  forall i j : nat,
    (S i + j) = S (i + j).
Proof.
  intros i j.
  ring.
Qed.

Lemma myst4v1_holds_specification :
  specification_of_the_mystery_function_4 myst4v1.
Proof.
  unfold specification_of_the_mystery_function_4.
  split.
    rewrite -> myst4v1_bc1.
    reflexivity.
    
    intros i j.
    induction i as [ | i' IHi'].
      rewrite -> myst4v1_bc1.
      rewrite ->2 plus_0_l.
      reflexivity.

      rewrite -> myst4v1_ic.
      rewrite -> S_outside_good.
      rewrite -> myst4v1_ic.
      exact IHi'.
Qed.

Fixpoint myst4v2 (x : nat) : nat :=
  match x with
    | 0 => 0
    | 1 => 0
    | (S (S x)) => myst4v2 x
  end.

Lemma myst4v2_bc1 :
  myst4v2 0 = 0.
Proof.
  unfold_tactic myst4v2.
Qed.

Lemma myst4v2_bc2 :
  myst4v2 1 = 0.
Proof.
  unfold_tactic myst4v2.
Qed.

Lemma myst4v2_ic :
  forall x : nat,
    myst4v2 (S (S x)) = myst4v2 x.
Proof.
  unfold_tactic myst4v2.
Qed.

Lemma myst4v2_holds_specification :
  specification_of_the_mystery_function_4 myst4v2.
Proof.
  unfold specification_of_the_mystery_function_4.
  split.
    rewrite -> myst4v2_bc1.
    reflexivity.
    
    intros i.
    induction i as [ | | i' IHi' IHSi'] using nat_ind2.
      intro j.
      rewrite -> myst4v2_bc1.
      rewrite ->2 plus_0_l.
      reflexivity.

      intro j.
      rewrite -> myst4v2_bc2.
      induction j as [ | j' IHj'].
        rewrite -> myst4v2_bc1.
        rewrite ->2 plus_0_r.
        rewrite -> myst4v2_bc2.
        reflexivity.

        rewrite <- S_equals_plus_1.
        rewrite -> myst4v2_ic.
        rewrite -> S_equals_plus_1.
        rewrite -> IHj'.
        rewrite ->2 plus_0_l.
        reflexivity.

      intro j.
      rewrite ->2 S_outside_good.
      rewrite -> plus_comm.
      rewrite <-2 S_outside_good.
      rewrite -> plus_comm.
      rewrite -> IHi'.
      rewrite ->2 myst4v2_ic.
      reflexivity.
Qed.

(* ********** *)

Definition specification_of_the_mystery_function_5 (f : nat -> nat) :=
  (f 0 = 0)
  /\
  (forall i : nat,
    f (S i) = S (2 * i + f i)).

Proposition there_is_only_one_mystery_function_5 :
  forall f g : nat -> nat,
    specification_of_the_mystery_function_5 f ->
    specification_of_the_mystery_function_5 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_5.
  intros [f_bc f_ic [g_bc g_ic]].
  intro n.
  induction n as [ | n' IHn'].
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.

    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> IHn'.
    reflexivity.
Qed.

Definition unit_test_for_the_mystery_function_5 (f : nat -> nat) :=
  (f 0 =n= 0)
  &&
  (f 1 =n= 1)
  &&
  (f 2 =n= 4)
  &&
  (f 3 =n= 9)
  &&
  (f 4 =n= 16)
  .

Definition square (x : nat) : nat :=
  x * x.

Compute unit_test_for_the_mystery_function_5 square.

Theorem and_the_mystery_function_5_is_dot_dot_dot :
  specification_of_the_mystery_function_5 square.
Proof.
  unfold specification_of_the_mystery_function_5.
  split.
    unfold square.
    ring.

    intros i.
    unfold square.
    ring.
Qed. 

(* ********** *)

Definition specification_of_the_mystery_function_6 (f : nat -> nat) :=
  (forall i j : nat,
    f (i + j) = f i + 2 * i * j + f j).

Definition myst6v1 (x : nat) : nat :=
  match x with
     | 0 => 0
     | (S x') => (S x') * (2 + x')
   end.

Lemma myst6v1_bc :
  myst6v1 0 = 0.
Proof.
  unfold_tactic myst6v1.
Qed.

Lemma myst6v1_ic :
  forall x : nat,
    myst6v1 (S x) = (S x) * (2 + x).
Proof.
  unfold_tactic myst6v1.
Qed.

Lemma myst6v1_holds_specification :
  specification_of_the_mystery_function_6 myst6v1.
Proof.
  unfold specification_of_the_mystery_function_6.
  intro i.

  induction i as [ | i' IHi'].
    intro j.
    rewrite -> myst6v1_bc.
    rewrite ->2 plus_0_l.
    reflexivity.

    intro j.
    rewrite -> S_outside_good.
    rewrite -> plus_comm.
    rewrite <- S_outside_good.
    rewrite -> plus_comm.
    rewrite -> myst6v1_ic.
    rewrite -> IHi'.
    rewrite -> myst6v1_ic.
    case j as [ | n].
      rewrite -> myst6v1_bc.
      case i' as [ | n].
        rewrite -> myst6v1_bc.
        ring.

        rewrite -> myst6v1_ic.
        ring.

    rewrite -> myst6v1_ic.
    case i' as [ | n'].
      rewrite -> myst6v1_bc.
      ring.

      rewrite -> myst6v1_ic.
      ring.
Qed.

Definition myst6v2 (x : nat) : nat :=
   match x with
     | 0 => 0
     | 1 => 2
     | S (S x'') => (S (S x'')) * (2 + S x'')
   end.

Lemma myst6v2_bc1 :
  myst6v2 0 = 0.
Proof.
  unfold_tactic myst6v2.
Qed.

Lemma myst6v2_bc2 :
  myst6v2 1 = 2.
Proof.
  unfold_tactic myst6v2.
Qed.

Lemma myst6v2_ic :
  forall x : nat,
    myst6v2 (S (S x)) = (S (S x)) * (2 + S x).
Proof.
  unfold_tactic myst6v2.
Qed.

Lemma myst6v2_holds_specification :
  specification_of_the_mystery_function_6 myst6v2.
Proof.
  unfold specification_of_the_mystery_function_6.
  intro i.

  induction i as [ | | i' IHi' IHSi'] using nat_ind2.
    intro j.
    rewrite -> myst6v2_bc1.
    rewrite ->2 plus_0_l.
    reflexivity.

    intro j.
    rewrite -> myst6v2_bc2.
    case j as [ | n].
      rewrite -> plus_0_r.
      rewrite -> myst6v2_bc1.
      rewrite -> myst6v2_bc2.
      ring.

      rewrite <- S_equals_plus_1.
      rewrite -> myst6v2_ic.
      case n as [ | n'].
        rewrite -> myst6v2_bc2.
        ring.

        rewrite -> myst6v2_ic.
        ring.

      intro j.
      rewrite ->2 S_outside_good.
      rewrite -> plus_comm.
      rewrite <-2 S_outside_good.
      rewrite -> plus_comm.
      rewrite -> IHi'.

      rewrite ->2 myst6v2_ic.
      case i' as [ | n].
        rewrite -> myst6v2_bc1.
        case j as [ | m].
          rewrite -> myst6v2_bc1.
          ring.

          case m as [ | m'].
            rewrite -> myst6v2_bc2.
            ring.

            rewrite -> myst6v2_ic.
            ring.
       
      case n as [ | n'].
        rewrite -> myst6v2_bc2.
        case j as [ | m].
          rewrite -> myst6v2_bc1.
          ring.

          case m as [ | m'].
            rewrite -> myst6v2_bc2.
            ring.

            rewrite -> myst6v2_ic.
            ring.

      rewrite -> myst6v2_ic.
      case j as [ | m].
        rewrite -> myst6v2_bc1.
        ring.

        case m as [ | m'].
          rewrite -> myst6v2_bc2.
          ring.

          rewrite -> myst6v2_ic.
          ring. 
Qed.

(* ********** *)

Definition specification_of_the_mystery_function_7 (f : nat -> nat) :=
  (f 0 = 1)
  /\
  (forall i j : nat,
    f (S (i + j)) = 2 * f i * f j).

Proposition there_is_only_one_mystery_function_7 :
  forall f g : nat -> nat,
    specification_of_the_mystery_function_7 f ->
    specification_of_the_mystery_function_7 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_7.
  intros [f_bc f_ic [g_bc g_ic]].
  intro n.
  induction n as [ | n' IHn'].
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.

    rewrite <- (plus_0_l n').
    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> IHn'.
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.
Qed.

Definition unit_test_for_the_mystery_function_7 (f : nat -> nat) :=
  (f 0 =n= 1)
  &&
  (f 1 =n= 2)
  &&
  (f 2 =n= 4)
  &&
  (f 3 =n= 8)
  &&
  (f 4 =n= 16)
  &&
  (f 5 =n= 32)
  .

Fixpoint binary_double (x : nat) : nat :=
  match x with
    | 0 => 1
    | (S x') => 2 * (binary_double x')
  end.

Compute unit_test_for_the_mystery_function_7 binary_double.

Require Import unfold_tactic.

Lemma binary_double_bc :
  binary_double 0 = 1.
Proof.
  unfold_tactic binary_double.
Qed.

Lemma binary_double_ic :
  forall x : nat,
    binary_double (S x) = 2 * (binary_double x).
Proof.
  unfold_tactic binary_double.
Qed.

Lemma succ_is_careless :
  forall i j : nat,
    S i + j = S (i + j).
Proof.
  intros i j.
  ring.
Qed.

Theorem and_the_mystery_function_7_is_dot_dot_dot :
  specification_of_the_mystery_function_7 binary_double.
Proof.
  unfold specification_of_the_mystery_function_7.
  split.
    exact binary_double_bc.

    intros i j.
    rewrite -> binary_double_ic.
    induction i as [ | i' IHi'].
      rewrite -> plus_0_l.
      rewrite -> binary_double_bc.
      ring.

      rewrite -> succ_is_careless.
      rewrite ->2 binary_double_ic.
      rewrite -> IHi'.
      ring.
Qed. 

(* ********** *)

Definition specification_of_the_mystery_function_8 (f : nat -> nat) :=
  (f 0 = 2)
  /\
  (forall i j : nat,
    f (S (i + j)) = f i * f j).

Proposition there_is_only_one_mystery_function_8 :
  forall f g : nat -> nat,
    specification_of_the_mystery_function_8 f ->
    specification_of_the_mystery_function_8 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_8.
  intros [f_bc f_ic [g_bc g_ic]].
  intro n.
  induction n as [ | n' IHn'].
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.

    rewrite <- (plus_0_l n').
    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> IHn'.
    rewrite -> f_bc.
    rewrite -> g_bc.
    reflexivity.
Qed.

Definition unit_test_for_the_mystery_function_8 (f : nat -> nat) :=
  (f 0 =n= 2)
  &&
  (f 1 =n= 4)
  &&
  (f 2 =n= 8)
  &&
  (f 3 =n= 16)
  &&
  (f 4 =n= 32)
  .

Fixpoint binary_double_2 (x : nat) : nat :=
  match x with
    | 0 => 2
    | (S x') => 2 * (binary_double_2 x')
  end.

Compute unit_test_for_the_mystery_function_8 binary_double_2.

Lemma binary_double_2_bc :
  binary_double_2 0 = 2.
Proof.
  unfold_tactic binary_double_2.
Qed.

Lemma binary_double_2_ic :
  forall x : nat,
    binary_double_2 (S x) = 2 * (binary_double_2 x).
Proof.
  unfold_tactic binary_double_2.
Qed.

Theorem and_the_mystery_function_8_is_dot_dot_dot :
  specification_of_the_mystery_function_8 binary_double_2.
Proof.
  unfold specification_of_the_mystery_function_8.
  split.
    exact binary_double_2_bc.

    intros i j.
    rewrite -> binary_double_2_ic.
    induction i as [ | i' IHi'].
      rewrite -> plus_0_l.
      rewrite -> binary_double_2_bc.
      ring.

      rewrite -> succ_is_careless.
      rewrite ->2 binary_double_2_ic.
      rewrite -> IHi'.
      ring.
Qed. 

(* ********** *)

Definition specification_of_the_mystery_function_9 (f : nat -> nat) :=
  (f 0 = 0)
  /\
  (f 1 = 1)
  /\
  (f 2 = 1)
  /\
  (forall p q : nat,
    f (S (p + q)) = f (S p) * f (S q) + f p * f q).


Proposition there_is_only_one_mystery_function_9 :
  forall f g : nat -> nat,
    specification_of_the_mystery_function_9 f ->
    specification_of_the_mystery_function_9 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_9.
  intros [f_bc0 [f_bc1 [f_bc2 f_ic]]].
  intros [g_bc0 [g_bc1 [g_bc2 g_ic]]].
  intro n.
  induction n as [ | | n' IHn' IHSn'] using nat_ind2.
    rewrite -> f_bc0.
    rewrite -> g_bc0.
    reflexivity.

    rewrite -> f_bc1.
    rewrite -> g_bc1.
    reflexivity.

    rewrite <- (plus_0_l n').
    rewrite <- (succ_is_careless 0 n').
    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> IHn'.
    rewrite -> IHSn'.
    rewrite -> f_bc1.
    rewrite -> g_bc1.
    rewrite -> f_bc2.
    rewrite -> g_bc2.
    rewrite ->2 mult_1_l.
    reflexivity.
Qed.

Definition unit_test_for_the_mystery_function_9 (f : nat -> nat) :=
  (f 0 =n= 0)
  &&
  (f 1 =n= 1)
  &&
  (f 2 =n= 1)
  &&
  (f 3 =n= 2)
  &&
  (f 4 =n= 3)
  &&
  (f 5 =n= 5)
  &&
  (f 6 =n= 8)
  .

Fixpoint fib (n:nat) : nat :=
  match n with
  | O => 0
  | (S n') =>
       match n' with
       | O => 1
       | (S n'') => (fib n'') + (fib n')
       end
  end.

Compute unit_test_for_the_mystery_function_9 fib.

Lemma fib_bc0 :
  fib 0 = 0.
Proof.
  unfold_tactic fib.
Qed.

Lemma fib_bc1 :
  fib 1 = 1.
Proof.
  unfold_tactic fib.
Qed.
  
Lemma fib_ic :
  forall x : nat,
    fib (S (S x)) = fib x + fib (S x).
Proof.
  unfold_tactic fib.
Qed.

Theorem and_the_mystery_function_9_is_dot_dot_dot :
  specification_of_the_mystery_function_9 fib.
Proof.
  unfold specification_of_the_mystery_function_9.
  split.
    exact fib_bc0.
  split.
    exact fib_bc1.
  split.
    rewrite -> fib_ic.
    rewrite -> fib_bc0.
    rewrite -> fib_bc1.
    ring.

  intro i.
  induction i as [ | i' IHi'].
    intro j.
    rewrite -> (fib_bc0).
    rewrite -> (fib_bc1).
    rewrite -> (mult_1_l).
    rewrite -> (mult_0_l).
    rewrite -> (plus_0_r).
    rewrite -> (plus_0_l).
    reflexivity.

    intro j.
    rewrite -> (succ_is_careless).
    rewrite -> plus_comm.
    rewrite <- (succ_is_careless).
    rewrite -> plus_comm.
    rewrite -> IHi'.
    rewrite -> (fib_ic).
    rewrite -> (fib_ic).
    ring.
Qed. 

(* ********** *)

Definition specification_of_the_mystery_function_10 (f : nat -> bool) :=
  (f 0 = true)
  /\
  (f 1 = false)
  /\
  (forall i j : nat,
     f (i + j) = eqb (f i) (f j)).

Proposition there_is_only_one_mystery_function_10 :
  forall f g : nat -> bool,
    specification_of_the_mystery_function_10 f ->
    specification_of_the_mystery_function_10 g ->
    forall n : nat,
      f n = g n.
Proof.
  intros f g.
  unfold specification_of_the_mystery_function_10.
  intros [f_bc0 [f_bc1 f_ic]].
  intros [g_bc0 [g_bc1 g_ic]].
  intro n.
  induction n as [ | | n' IHn' IHSn'] using nat_ind2.
    rewrite -> f_bc0.
    rewrite -> g_bc0.
    reflexivity.

    rewrite -> f_bc1.
    rewrite -> g_bc1.
    reflexivity.

    rewrite <- (plus_0_l n').
    rewrite <- (succ_is_careless).
    rewrite -> plus_comm.
    rewrite <- (succ_is_careless).
    rewrite -> f_ic.
    rewrite -> g_ic.
    rewrite -> IHSn'.
    rewrite -> f_bc1.
    rewrite -> g_bc1.
    reflexivity.
Qed.

Definition unit_test_for_the_mystery_function_10 (f : nat -> bool) :=
  (f 0 =b= true)
  &&
  (f 1 =b= false)
  &&
  (f 2 =b= true)
  &&
  (f 3 =b= false)
  &&
  (f 4 =b= true)
  &&
  (f 5 =b= false)
  &&
  (f 6 =b= true)
  .

Fixpoint is_even (x : nat) : bool :=
  match x with
  | 0 => true
  | 1 => false
  | (S (S x')) => (is_even x')
  end.

Compute unit_test_for_the_mystery_function_10 is_even.

Lemma is_even_bc0 :
  is_even 0 = true.
Proof.
  unfold_tactic is_even.
Qed.

Lemma is_even_bc1 :
  is_even 1 = false.
Proof.
  unfold_tactic is_even.
Qed.

Lemma is_even_ic :
  forall x : nat,
    is_even (S (S x)) = is_even x.
Proof.
  unfold_tactic is_even.
Qed.

Lemma truth_be_told :
  forall j : nat,
    is_even j = (true =b= is_even j).
Proof.
  intro j.
  induction j as [ | | j' IHj' IHSj'] using nat_ind2.
    rewrite -> is_even_bc0.
    ring.

    rewrite -> is_even_bc1.
    ring.

    rewrite -> is_even_ic.
    exact IHj'.
Qed.

Lemma truth_be_told2 :
  forall j : nat,
    is_even (S j) = (false =b= is_even j).
Proof.
  intro j.
  induction j as [ | | j' IHj' IHSj'] using nat_ind2.
    rewrite -> is_even_bc0.
    ring.

    rewrite -> is_even_bc1.
    ring.

    rewrite -> is_even_ic.
    exact IHj'.
Qed.

Lemma swap_symmetry :
  forall a b : bool,
    (a =b= b) = (b =b= a).
Proof.
  intros a b.
  unfold eqb.
  induction a as [ | ].
    reflexivity.
    reflexivity.
Qed.

Theorem and_the_mystery_function_10_is_dot_dot_dot :
  specification_of_the_mystery_function_10 is_even.
Proof.
  unfold specification_of_the_mystery_function_10.
  split.
    exact is_even_bc0.
  split.
    exact is_even_bc1.
  intro i.
  induction i as [ | | i' IHi' IHSi'] using nat_ind2.
    intro j.
    rewrite -> (is_even_bc0).
    rewrite -> (plus_0_l).
    apply truth_be_told.

    intro j.
    rewrite -> is_even_bc1.
    case j as [ | n].
      rewrite -> plus_0_r.
      rewrite <- truth_be_told2.
      reflexivity.
      
      rewrite <- S_equals_plus_1.
      rewrite <- truth_be_told2.
      reflexivity.

    intro j.
    rewrite -> S_equals_plus_1 at 1.
    rewrite -> (plus_comm 1 (S i')).
    rewrite -> S_equals_plus_1 at 1.
    rewrite -> (plus_comm 1 i').
    rewrite <- (plus_assoc).
    rewrite <- (S_equals_plus_1 j).
    rewrite <- (plus_assoc).
    rewrite <- (S_equals_plus_1).
    rewrite -> (IHi').
    rewrite ->2 (is_even_ic).
    reflexivity.
Qed.
(* ********** *)

(* end of week_38c_mystery_functions_after_todays_lecture.v *)
