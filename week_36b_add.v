(* week_36b_add.v *)
(* dIFP 2014-2015, Q1, Week 36 *)
(* Olivier Danvy <danvy@cs.au.dk> *)

(* ********** *)

(* Learning goals of this week:

   * using coqc

   * unit tests

   * booleans

   * infix notation

   * specifications of functions

   * properties of functions that satisfy a specification

   * the unfold tactic to replace an identifier by its denotation

   * structural induction over natural numbers

   * revert as an inverse of intro

   * uniqueness of a specification

   * structurally recursive definitions over natural numbers

   * unfold lemmas

   * unfold_tactic

   * checking than an implementation satisfies a specification

*)

(* ********** *)

Require Import Arith.

(* ********** *)

Definition unit_tests_for_addition_v0 (add : nat -> nat -> nat) :=
  (add 0 3 = 3)
  /\
  (add 1 3 = 4)
  /\
  (add 2 3 = 5)
  /\
  (add 3 0 = 3).

(* ********** *)

(* plus is Coq's built-in addition function (infix notation: +): *)

Check plus.
(*
plus
     : nat -> nat -> nat
*)

Compute (unit_tests_for_addition_v0 plus).
(*
     = 3 = 3 /\ 4 = 4 /\ 5 = 5 /\ 3 = 3
     : Prop
*)

(* That's unpractical.
   Let's rather compute over Booleans: *)

Require Import Bool.

Check true.
(*
true
     : bool
*)

Check false.
(*
false
     : bool
*)

(* andb is Coq's built-in Boolean conjunction function (infix notation: &&): *)

Check andb.
(*
andb
     : bool -> bool -> bool
*)

Compute (andb true true, andb true false, andb false false, andb false false).
(*
     = (true, false, false, false)
     : bool * bool * bool * bool
*)

Compute (true && true, true && false, false && false, false && false).
(*
     = (true, false, false, false)
     : bool * bool * bool * bool
*)

(* orb is Coq's built-in Boolean conjunction function (infix notation: ||): *)

Check orb.
(*
orb
     : bool -> bool -> bool
*)

Compute (orb true true, orb true false, orb false false, orb false false).
(*
     = (true, true, false, false)
     : bool * bool * bool * bool
*)

Compute (true || true, true || false, false || false, false || false).
(*
     = (true, true, false, false)
     : bool * bool * bool * bool
*)

(* The equality predicate for natural numbers: *)

Check eq_nat.
(*
eq_nat
     : nat -> nat -> Prop
*)

Compute (eq_nat 2 2, eq_nat 2 3).
(*
     = (True, False)
     : Prop * Prop
*)

(* The equality Boolean function for natural numbers: *)

Check beq_nat.
(*
beq_nat
     : nat -> nat -> bool
*)

Compute (beq_nat 2 2, beq_nat 2 3).
(*
     = (true, false)
     : bool * bool
*)

(* A second version of the unit tests for addition: *)

Definition unit_tests_for_addition_v1 (add : nat -> nat -> nat) :=
  (beq_nat (add 0 3) 3)
  &&
  (beq_nat (add 1 3) 4)
  &&
  (beq_nat (add 2 3) 5)
  &&
  (beq_nat (add 3 0) 3).

Compute (unit_tests_for_addition_v1 plus).
(*
     = true
     : bool
*)

(* A bit of syntactic sugar: *)

Notation "A === B" := (beq_nat A B) (at level 70, right associativity).

(* Now the unit tests are even readable: *)

Definition unit_tests_for_addition_v2 (add : nat -> nat -> nat) :=
  (add 0 3 === 3)
  &&
  (add 1 3 === 4)
  &&
  (add 2 3 === 5)
  &&
  (add 3 0 === 3).

Compute (unit_tests_for_addition_v2 plus).
(*
     = true
     : bool
*)

(* Yup, readable and practical: *)

Definition unit_tests_for_addition := unit_tests_for_addition_v2.

Compute (unit_tests_for_addition plus).
(*
     = true
     : bool
*)

(* ********** *)

(* A useful lemma: *)

Lemma f_equal_S :
  forall x y : nat,
    x = y -> S x = S y.
Proof.
  intros x y.
  intro x_equals_y.
  destruct x_equals_y.
  reflexivity.
Qed.

(* ********** *)

(* The specification of the addition function, 
   by induction over natural numbers:
*)

Definition specification_of_addition (add : nat -> nat -> nat) :=
  (forall j : nat,
    add O j = j)
  /\
  (forall i' j : nat,
    add (S i') j = S (add i' j)).

(* ********** *)

(* A collection of properties
   about a function that satisfies the specification of addition:
*)

Proposition addition_bc_left :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall j : nat,
      add 0 j = j.
Proof.
  intros add specification_of_addition.
  intro j.
  destruct specification_of_addition as [add_base add_induction].
  apply add_base.
Qed.

(* ********** *)

Proposition addition_ic_left :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i' j : nat,
      add (S i') j = S (add i' j).
Proof.
  intros add specification_of_add.
  intros i' j.
  destruct specification_of_add as [add_base add_induction].
  apply add_induction.
Qed.

Corollary addition_ic_left_twice :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i'' j : nat,
      add (S (S i'')) j = S (S (add i'' j)).
Proof.
  intros add specification_of_add.
  intros i'' j'.
  rewrite (addition_ic_left add specification_of_add (S i'') j').
  apply f_equal_S.
  rewrite (addition_ic_left add specification_of_add i'' j').
  reflexivity.
Qed.

(* ********** *)

Proposition addition_bc_right :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i : nat,
      add i 0 = i.
Proof.
  intros add spec_add.
  intro i.
  destruct spec_add as [add_b add_i].
  induction i as [ | n].
  apply (add_b 0).
  rewrite -> (add_i n 0).
  apply f_equal_S.
  apply IHn.
Qed.

(* ********** *)

Proposition addition_ic_right :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i j : nat,
      add i (S j) = S (add i j).
Proof.
  intros add spec_add.
  intros i j.
  induction i as [ | n].
  rewrite -> (addition_bc_left add spec_add (S j)).
  apply f_equal_S.
  symmetry.
  apply (addition_bc_left add spec_add j).
  
  rewrite -> (addition_ic_left add spec_add n (S j)).
  apply f_equal_S.
  rewrite -> (addition_ic_left add spec_add n j).
  rewrite -> (IHn).
  reflexivity.
Qed.

Corollary addition_ic_right_twice :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i j : nat,
      add i (S (S j)) = S (S (add i j)).
Proof.
  intros add spec_add.
  intros i j.
  rewrite -> (addition_ic_right add spec_add i (S j)).
  apply f_equal_S.
  rewrite -> (addition_ic_right add spec_add i j).
  reflexivity.
Qed.

(* ********** *)

(* Exercise:
   state and prove that zero is left-neutral for addition.
*)

Proposition zero_is_neutral_for_addition_on_the_left :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i : nat,
      add 0 i = i.
Proof.
  intros add spec_add.
  intro i.
  induction i as [ | n].
  destruct spec_add as [add_b add_i].
  apply (add_b 0). 
  rewrite -> (addition_ic_right add spec_add 0 n).
  rewrite -> (IHn).
  reflexivity.
Qed.

(* ********** *)

(* Exercise:
   state and prove that zero is right-neutral for addition.
*)

Proposition zero_is_neutral_for_addition_on_the_right :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i : nat,
      add i 0 = i.
Proof.
  intros add spec_add.
  intro i.
  induction i as [ | n].
  destruct spec_add as [add_b add_i].
  apply (add_b 0).
  
  rewrite -> (addition_ic_left add spec_add n 0).
  apply (f_equal_S).
  apply (IHn).
Qed.

(* ********** *)

Proposition addition_is_commutative :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i j : nat,
      add i j = add j i.
Proof.
  intros add spec_add.
  intros i j.
  induction i as [ | n].
  induction j as [ | n].
  reflexivity.
  
  rewrite -> (addition_ic_left add spec_add n 0).
  rewrite -> (addition_ic_right add spec_add 0 n).
  apply (f_equal S).
  apply (IHn).

  rewrite -> (addition_ic_left add spec_add n j).
  rewrite -> (addition_ic_right add spec_add j n).
  apply (f_equal S).
  apply (IHn).
Qed.

(* ********** *)

Proposition addition_is_associative :
  forall (add : nat -> nat -> nat),
    specification_of_addition add ->
    forall i j k : nat,
      add i (add j k) = add (add i j) k.
Proof.
  intros add spec_add.
  intros i j k.
  induction i as [ | n].
  induction j as [ | n].
  induction k as [ | n].

  destruct spec_add as [add_b add_i].
  rewrite -> (add_b (add 0 0)).
  rewrite -> (add_b 0).
  symmetry.
  apply (add_b 0).

  rewrite -> (addition_ic_right add spec_add 0 n).
  rewrite -> (addition_ic_right add spec_add 0 (add 0 n)).
  rewrite -> (addition_ic_right add spec_add (add 0 0) n).
  apply (f_equal_S).
  apply (IHn).

  rewrite -> (addition_ic_left add spec_add n k).
  rewrite -> (addition_ic_right add spec_add 0 (add n k)).
  rewrite -> (addition_ic_right add spec_add 0 n). 
  rewrite -> (addition_ic_left add spec_add (add 0 n) k).
  apply (f_equal_S).
  apply (IHn).

  rewrite -> (addition_ic_left add spec_add n (add j k)).
  rewrite -> (addition_ic_left add spec_add n j).
  rewrite -> (addition_ic_left add spec_add (add n j) k).
  apply (f_equal_S).
  apply (IHn).
Qed.

(* ********** *)

Proposition there_is_only_one_addition :
  forall add1 add2 : nat -> nat -> nat,
    specification_of_addition add1 ->
    specification_of_addition add2 ->
    forall m n : nat,
      add1 m n = add2 m n.
Proof.
  intros add1 add2 spec_add1 spec_add2.
  intros i j.
  induction i as [ | n].
  induction j as [ | n].

  destruct spec_add1 as [add1_b add1_h].
  destruct spec_add2 as [add2_b add2_h].
  rewrite -> (add1_b 0).
  rewrite -> (add2_b 0).
  reflexivity.

  rewrite -> (addition_ic_right add1 spec_add1 0 n).
  rewrite -> (addition_ic_right add2 spec_add2 0 n).
  apply (f_equal_S).
  apply (IHn).

  rewrite -> (addition_ic_left add1 spec_add1 n j).
  rewrite -> (addition_ic_left add2 spec_add2 n j).
  apply (f_equal_S).
  apply (IHn).
Qed.

(* ********** *)

Fixpoint add_v1 (i j : nat) : nat :=
  match i with
    | 0 => j
    | S i' => S (add_v1 i' j)
  end.

Compute (unit_tests_for_addition add_v1).
(*
     = true
     : bool
*)

Require Import "unfold_tactic".

(* The two mandatory unfold lemmas: *)

Lemma unfold_add_v1_bc :
  forall j : nat,
    add_v1 0 j = j.
Proof.
  unfold_tactic add_v1.
Qed.

Lemma unfold_add_v1_ic :
  forall i' j : nat,
    add_v1 (S i') j = S (add_v1 i' j).
Proof.
  unfold_tactic add_v1.
Qed.

Theorem add_v1_satisfies_the_specification_of_addition :
  specification_of_addition add_v1.
Proof.
  unfold specification_of_addition.
  split.

  intro j.
  apply (unfold_add_v1_bc).

  intros i j.
  apply (unfold_add_v1_ic).
Qed.

(* ********** *)

Fixpoint add_v2 (i j : nat) : nat :=
  match i with
    | 0 => j
    | S i' => add_v2 i' (S j)
  end.

Compute (unit_tests_for_addition add_v2).
(*
     = true
     : bool
*)

(* The two mandatory unfold lemmas: *)

Lemma unfold_add_v2_bc :
  forall j : nat,
    add_v2 0 j = j.
Proof.
  unfold_tactic add_v2.
Qed.

Lemma unfold_add_v2_ic :
  forall i' j : nat,
    add_v2 (S i') j = add_v2 i' (S j).
Proof.
  unfold_tactic add_v2.
Qed.

(* A useful lemma: *)

Proposition add_v2_ic_right :
  forall i j : nat,
    add_v2 i (S j) = S (add_v2 i j).
Proof.
  intro i.
  induction i as [ | n].
  intro j.
  rewrite -> (unfold_add_v2_bc (S j)).
  rewrite -> (unfold_add_v2_bc j).
  reflexivity.

  intro j.
  rewrite -> (unfold_add_v2_ic n j).
  rewrite -> (unfold_add_v2_ic n (S j)).
  rewrite -> (IHn).
  reflexivity.
Qed.

Theorem add_v2_satisfies_the_specification_of_addition :
  specification_of_addition add_v2.
Proof.
  unfold specification_of_addition.
  split.

  intro j.
  apply (unfold_add_v2_bc j).

  intros i j.
  apply (add_v2_ic_right i j).
Qed.

(* ********** *)

Theorem functional_equality_of_add_v1_and_add_v2 :
  forall i j : nat,
    add_v1 i j = add_v2 i j.
Proof.
  intro i.
  induction i as [ | n].
  intro j.
  rewrite -> (unfold_add_v1_bc j).
  rewrite -> (unfold_add_v2_bc j).
  reflexivity.

  intro j.
  rewrite -> (unfold_add_v1_ic n j).
  rewrite -> (IHn).
  rewrite -> (unfold_add_v2_ic n j).
  rewrite -> (add_v2_ic_right n j).
  reflexivity.
Qed.

(* ********** *)

(* end of week_36b_add.v *)
