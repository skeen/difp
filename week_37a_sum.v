(* week_37a_sum.v *)
(* dIFP 2014-2015, Q1, Week 37 *)
(* Olivier Danvy <danvy@cs.au.dk> *)

(* ********** *)

(* The goal of this file is to study the sum function:
     sum f n = f 0 + f 1 + ... + f n
*)

(* ********** *)

Require Import Arith Bool.

Require Import unfold_tactic.

(* ********** *)

(* The canonical unfold lemmas
   associated to plus and mult,
   which are predefined:
*)

Lemma unfold_plus_bc :
  forall y : nat,
    0 + y = y.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic plus.
Qed.

Lemma unfold_plus_ic :
  forall x' y : nat,
    (S x') + y = S (x' + y).
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic plus.
Qed.

Lemma unfold_mult_bc :
  forall y : nat,
    0 * y = 0.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic mult.
Qed.

Lemma unfold_mult_ic :
  forall x' y : nat,
    (S x') * y = y + (x' * y).
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic mult.
Qed.

(* ********** *)

Notation "A === B" := (beq_nat A B) (at level 70, right associativity).

Definition unit_tests_for_sum (sum : (nat -> nat) -> nat -> nat) :=
  (sum (fun n => 0) 0 === 0)
  &&
  (sum (fun n => 0) 1 === 0 + 0)
  &&
  (sum (fun n => 0) 2 === 0 + 0 + 0)
  &&
  (sum (fun n => 1) 0 === 1)
  &&
  (sum (fun n => 1) 1 === 1 + 1)
  &&
  (sum (fun n => 1) 2 === 1 + 1 + 1)
  &&
  (sum (fun n => n) 0 === 0)
  &&
  (sum (fun n => n) 1 === 0 + 1)
  &&
  (sum (fun n => n) 2 === 0 + 1 + 2)
  &&
  (sum (fun n => n * n) 0 === 0 * 0)
  &&
  (sum (fun n => n * n) 1 === 0 * 0 + 1 * 1)
  &&
  (sum (fun n => n * n) 2 === 0 * 0 + 1 * 1 + 2 * 2)
  &&
  (sum (fun n => n * n) 3 === 0 * 0 + 1 * 1 + 2 * 2 + 3 * 3)
  &&
  (sum S 0 === 1)
  &&
  (sum S 1 === 1 + 2)
  &&
  (sum S 2 === 1 + 2 + 3)
  .

(* Exercise: add some more tests to this unit test. *)

(* ********** *)

Definition specification_of_sum (sum : (nat -> nat) -> nat -> nat) :=
  forall f : nat -> nat,
    sum f 0 = f 0
    /\
    forall n' : nat,
      sum f (S n') = sum f n' + f (S n').

(* ********** *)

Theorem there_is_only_one_sum :
  forall sum1 sum2 : (nat -> nat) -> nat -> nat,
    specification_of_sum sum1 ->
    specification_of_sum sum2 ->
    forall (f : nat -> nat)
           (n : nat),
      sum1 f n = sum2 f n.
Proof.
  intros sum1 sum2.
  intros s_sum1 s_sum2.
  induction n as [ | n' IHn'].
    unfold specification_of_sum in s_sum1.
    unfold specification_of_sum in s_sum2.
    Check (s_sum1 f).
    destruct (s_sum1 f) as [s_sum1_b s_sum1_i].
    destruct (s_sum2 f) as [s_sum2_b s_sum2_i].
    clear s_sum1.
    clear s_sum2.
    rewrite -> s_sum1_b.
    rewrite -> s_sum2_b.
    reflexivity.

    unfold specification_of_sum in s_sum1.
    unfold specification_of_sum in s_sum2.
    Check (s_sum1 f).
    destruct (s_sum1 f) as [s_sum1_b s_sum1_i].
    destruct (s_sum2 f) as [s_sum2_b s_sum2_i].
    clear s_sum1.
    clear s_sum2.
    Check (s_sum1_i n').
    rewrite -> (s_sum1_i n').
    rewrite -> (s_sum2_i n').
    rewrite -> (IHn').
    reflexivity.
Qed.

(* ********** *)

(* Misc. instances of the sum function: *)

Lemma about_sum_0 :
  forall sum : (nat -> nat) -> nat -> nat,
    specification_of_sum sum ->
    forall n : nat,
      sum (fun i => 0) n = 0.
Proof.
  intro sum.
  intro s_sum.
  intro n.
  induction n as [ | n' IHn'].
    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun _ : nat => 0)) as [sum_b sum_h].
    clear s_sum.
    apply (sum_b).

    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun _ : nat => 0)) as [sum_b sum_h].
    clear s_sum.
    rewrite -> (sum_h n').
    rewrite -> (IHn').
    rewrite -> (plus_0_l 0).
    reflexivity.
Qed.

(* ***** *)


Lemma one_plus_i_equals_S_i :
  forall i : nat,
    plus 1 i = (S i).
Proof.
  intro i.
  unfold plus.
  reflexivity.
Qed.

Lemma about_sum_1 :
  forall sum : (nat -> nat) -> nat -> nat,
    specification_of_sum sum ->
    forall n : nat,
      sum (fun i => 1) n = S n.
Proof.
  intro sum.
  intro s_sum.
  intro n.
  induction n as [ | n' IHn'].
    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun i => 1)) as [sum_b sum_h].
    clear s_sum.
    apply (sum_b).

    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun i => 1)) as [sum_b sum_h].
    clear s_sum.
    rewrite -> (sum_h n').
    rewrite -> (IHn').
    rewrite <- (one_plus_i_equals_S_i (S n')).
    rewrite -> (plus_comm (S n') 1).
    reflexivity.
Qed.

(* ***** *)

Lemma two_times_the_same_thing_is_the_same_thing :
  forall i : nat,
    i + i = 2 * i.
Proof.
  intro i.
  induction i as [ | n' IHn'].
    rewrite (plus_0_l 0).
    rewrite (mult_0_r 2).
    reflexivity.

    rewrite (mult_succ_l 1 (S n')).
    rewrite (mult_1_l (S n')).
    reflexivity.
Qed.

Lemma about_sum_identity :
  forall sum : (nat -> nat) -> nat -> nat,
    specification_of_sum sum ->
    forall n : nat,
      2 * sum (fun i => i) n = n * S n.
Proof.
  intro sum.
  intro s_sum.
  intro n.
  induction n as [ | n' IHn'].
     unfold specification_of_sum in s_sum.
     destruct (s_sum (fun i => i)) as [sum_b sum_h].
     clear s_sum.
     rewrite (sum_b).
     rewrite (mult_0_r 2).
     rewrite (mult_0_l 1).
     reflexivity.

     unfold specification_of_sum in s_sum.
     destruct (s_sum (fun i => i)) as [sum_b sum_h].
     clear s_sum.
     rewrite -> (sum_h n').
     rewrite -> (mult_plus_distr_l 2 (sum (fun i : nat => i) n') (S n')).
     rewrite -> (IHn').
     rewrite -> (mult_succ_r (S n') (S n')).
     rewrite -> (mult_succ_r (S n') n').
     rewrite -> (mult_comm (S n') (n')).
     rewrite <- (two_times_the_same_thing_is_the_same_thing (S n')).
     rewrite -> (plus_assoc (n' * S n') (S n') (S n')).
     reflexivity.
Qed.

(* ***** *)

Lemma about_sum_even_numbers :
  forall sum : (nat -> nat) -> nat -> nat,
    specification_of_sum sum ->
    forall n : nat,
      sum (fun i => 2 * i) n = n * S n.
Proof.
  intro sum.
  intro s_sum.
  intro n.
  induction n as [ | n' IHn'].
    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun i => 2 * i)) as [sum_b sum_h].
    clear s_sum.
    rewrite -> (mult_0_l 1).
    rewrite -> (sum_b).
    rewrite -> (mult_0_r 2).
    reflexivity.
    
    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun i => 2 * i)) as [sum_b sum_h].
    clear s_sum.
    rewrite -> (sum_h n').
    rewrite -> (IHn').
    rewrite -> (mult_succ_r (S n') (S n')).
    rewrite -> (mult_succ_r (S n') n').
    rewrite -> (mult_comm (S n') (n')).
    rewrite <- (two_times_the_same_thing_is_the_same_thing (S n')).
    rewrite -> (plus_assoc (n' * S n') (S n') (S n')).
    reflexivity.
Qed.

(* ***** *)

Lemma a_humble_little_lemma :
  forall a b c : nat,
    a + b + b + c = a + 2 * b + c.
Proof.
  intros a b c.
  rewrite -> (unfold_mult_ic 1 b).
  rewrite -> (unfold_mult_ic 0 b).
  rewrite -> (unfold_mult_bc b).
  rewrite -> plus_0_r.
  rewrite -> plus_assoc.
  reflexivity.
Qed.

Lemma binomial_2 :
  forall x y : nat,
    (x + y) * (x + y) = x * x + 2 * x * y + y * y.
Proof.
  intros x y.
  induction x as [ | x' IHx'].
    rewrite -> (plus_0_l y).
    rewrite -> (mult_0_l 0).
    rewrite -> (mult_0_r 2).
    rewrite -> (mult_0_l y).
    rewrite -> (plus_0_l 0).
    rewrite -> (plus_0_l (y * y)).
    reflexivity.

    rewrite <- (one_plus_i_equals_S_i x').
    rewrite <- (plus_assoc 1 x' y).
    rewrite -> (mult_plus_distr_r 1 (x' + y) (1 + (x' + y))).
    rewrite -> (mult_1_l (1 + (x' + y))).
    rewrite -> (mult_plus_distr_l (x' + y) 1 (x' + y)).
    rewrite -> (mult_1_r (x' + y)).

    rewrite -> (IHx').
    (* Clean up all parantheses *)
    rewrite -> (plus_assoc 1 x' y).
    rewrite -> (plus_assoc (1 + x' + y) (x'+y) (x' * x' + 2 * x' * y + y * y)).
    rewrite -> (plus_assoc (1 + x' + y) x' y).
    rewrite -> (mult_plus_distr_l 2 1 x').
    rewrite -> (mult_plus_distr_l (1 + x') 1 x').
    rewrite -> (mult_1_r (1 + x')).
    rewrite -> (mult_1_r 2).
    rewrite -> (mult_plus_distr_r 1 x' x').
    rewrite -> (mult_1_l x').
    rewrite -> (mult_plus_distr_r 2 (2*x') y).
    rewrite -> (plus_assoc (1 + x') x' (x' * x')).
    rewrite -> (plus_assoc (1 + x' + x' + x' * x') (2 * y) (2 * x' * y)).
    rewrite <- (plus_assoc (x' * x') (2 * x' * y) (y*y)).
    rewrite -> (plus_assoc (1 + x' + y + x' + y) (x'*x') (2 * x' * y + y*y)).
    rewrite -> (plus_assoc (1 + x' + y + x' + y + x'*x') (2*x'*y) (y*y)).
    rewrite <- (two_times_the_same_thing_is_the_same_thing y).
    rewrite -> (plus_assoc (1 + x' + x' + x' * x') y y).
    rewrite -> (plus_comm (1 + x') y).
    rewrite -> (plus_comm (y + (1 + x')) x').
    rewrite -> (plus_comm (x' + (y + (1 +x'))) y).
    rewrite -> (plus_comm (y + (x' + (y + (1 + x')))) (x' * x')).

    rewrite -> (plus_comm (1 + x') x').
    rewrite -> (plus_comm (x' + (1 + x')) (x'*x')).
    rewrite -> (plus_comm (x'*x' + (x' + (1 + x'))) y).
    rewrite -> (plus_comm (y + (x'*x' + (x' + (1 + x')))) y).

    rewrite -> (plus_assoc y y (x' * x' + (x' + (1 + x')))).
    rewrite -> (plus_assoc (y+y) (x' * x') (x' + (1 + x'))).
    rewrite -> (plus_comm (y + y) (x' * x')).

    rewrite -> (plus_assoc (x' * x') y y).
    rewrite -> (plus_assoc (x' * x') y (x' + (y + (1+x')))).
    rewrite -> (plus_assoc (x' * x' + y) x' (y + (1+x'))).
    rewrite -> (plus_assoc (x' * x' + y + x') y (1+x')).
    rewrite -> (plus_assoc (x' * x' + y + y) x' (1+x')).

    rewrite -> (plus_comm (x' * x' + y) x').
    rewrite -> (plus_comm (x' * x' + y) y).
    rewrite -> (plus_assoc x' (x' * x') y).
    rewrite -> (plus_assoc y (x' * x') y).
    rewrite -> (plus_comm (x' + x' * x' + y) y).
    rewrite -> (plus_comm (y + x' * x' + y) x').
    rewrite -> (plus_assoc y (x' + x'*x') y).
    rewrite -> (plus_assoc x' (y + x'*x') y).
    rewrite -> (plus_assoc y x' (x'*x')).
    rewrite -> (plus_assoc x' y (x'*x')).
    rewrite -> (plus_comm y x').
    reflexivity.
Qed.

Lemma about_sum_odd_numbers :
  forall sum : (nat -> nat) -> nat -> nat,
    specification_of_sum sum ->
    forall n : nat,
      sum (fun i => S (2 * i)) n = S n * S n.
Proof.
  intro sum.
  intro s_sum.
  intro n.
  induction n as [ | n' IHn'].
    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun i : nat => S (2 * i))) as [sum_b sum_h].
    clear s_sum.
    rewrite -> (sum_b).
    rewrite -> (mult_0_r 2).
    rewrite -> (mult_1_r 1).
    reflexivity.

    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun i : nat => S (2 * i))) as [sum_b sum_h].
    clear s_sum.
    rewrite -> (sum_h n').
    rewrite -> (IHn').
    rewrite -> (mult_succ_r (S (S n')) (S n')).
    rewrite -> (mult_succ_l (S n') (S n')).
    rewrite <- (one_plus_i_equals_S_i (S n')).
    rewrite <- (plus_assoc (S n' * S n') (S n') (1 + (S n'))).

    rewrite -> (plus_comm (S n' * S n') (S (2 * S n'))).
    rewrite -> (plus_comm (S n' * S n') (S n' + (1 + S n'))).
    rewrite <- (one_plus_i_equals_S_i (2 * (S n'))).
    rewrite -> (plus_comm (S n') (1 + S n')).
    rewrite <- (two_times_the_same_thing_is_the_same_thing (S n')).
    rewrite -> (plus_assoc 1 (S n') (S n')).
    reflexivity.
Qed.

(* ***** *)

(* From the June exam of dProgSprog 2012-2013: *)

Lemma factor_sum_on_the_left :
  forall sum : (nat -> nat) -> nat -> nat,
    specification_of_sum sum ->
    forall (h : nat -> nat)
           (c k : nat),
      sum (fun x => c * h x) k = c * sum (fun x => h x) k.
Proof.
  intro sum.
  intro s_sum.
  intro h.
  intros c k.
  
  unfold specification_of_sum in s_sum.
  destruct (s_sum (fun x : nat => c * h x)) as [sum_1b sum_1h].
  destruct (s_sum (fun x : nat => h x)) as [sum_2b sum_2h].
  clear s_sum.
  induction k as [ | k' IHk'].
    rewrite -> (sum_1b).
    rewrite -> (sum_2b).
    reflexivity.

    rewrite -> (sum_1h k').
    rewrite -> (sum_2h k').
    rewrite -> (mult_plus_distr_l c (sum (fun x : nat => h x) k') (h (S k'))).
    rewrite -> (IHk').
    reflexivity.
Qed.

Lemma factor_sum_on_the_right :
  forall sum : (nat -> nat) -> nat -> nat,
    specification_of_sum sum ->
    forall (h : nat -> nat)
           (c k : nat),
      sum (fun x => h x * c) k = (sum (fun x => h x) k) * c.
Proof.
  intro sum.
  intro s_sum.
  intro h.
  intros c k.
  
  unfold specification_of_sum in s_sum.
  destruct (s_sum (fun x => h x * c)) as [sum_1b sum_1h].
  destruct (s_sum (fun x => h x)) as [sum_2b sum_2h].
  clear s_sum.
  induction k as [ | k' IHk'].
     rewrite -> (sum_1b).  
     rewrite -> (sum_2b).
     reflexivity.

     rewrite -> (sum_1h k').
     rewrite -> (sum_2h k').
     rewrite -> (mult_plus_distr_r (sum (fun x : nat => h x) k') (h (S k')) c).
     rewrite -> (IHk').
     reflexivity.
Qed.

Theorem June_exam :
  forall sum : (nat -> nat) -> nat -> nat,
    specification_of_sum sum ->
    forall (f g : nat -> nat)
           (m n : nat),
      sum (fun i => sum (fun j => f i * g j) n) m =
      (sum (fun i => f i) m) * (sum (fun j => g j) n).
Proof.
  intro sum.
  intro s_sum.
  intros f g.
  intros m n.
(*
Left
      sum (fun x => c * h x) k = c * sum (fun x => h x) k.
Right
      sum (fun x => h x * c) k = (sum (fun x => h x) k) * c.
*)
  assert (s_sum' := s_sum).

  induction m as [ | m' IHm'].
    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun i : nat => sum (fun j : nat => f i * g j) n)) as [sum_1b _].
    rewrite -> (sum_1b).
    rewrite -> (factor_sum_on_the_left sum s_sum' (fun j : nat => g j) (f 0) n).
    destruct (s_sum (fun i : nat => f i)) as [sum_2b _].
    rewrite -> (sum_2b).
    reflexivity.

    unfold specification_of_sum in s_sum.
    destruct (s_sum (fun i : nat => sum (fun j : nat => f i * g j) n)) as [_ sum_1h].
    rewrite -> (sum_1h m').
    rewrite -> (IHm').
    clear IHm'.
    clear sum_1h.
    destruct (s_sum (fun i : nat => f i)) as [_ sum_1h].
    rewrite -> (sum_1h m').
    clear sum_1h.
    rewrite -> (mult_plus_distr_r (sum (fun i : nat => f i) m') (f (S m')) (sum (fun j : nat => g j) n)).
    rewrite <- (factor_sum_on_the_left sum s_sum' (fun j : nat => g j) (f (S m')) n).
    reflexivity.
Qed.

(* ********** *)

(* Food for thought:
   is the following specification of sum
   equivalent to the one above?
*)

Definition alt_specification_of_sum (sum : (nat -> nat) -> nat -> nat) :=
  (forall f : nat -> nat,
    sum f 0 = f 0)
  /\
  (forall (f : nat -> nat)
          (n' : nat),
     sum f (S n') = sum f n' + f (S n')).

Theorem specifications_are_equivalent : 
  forall sum1 sum2 : (nat -> nat) -> nat -> nat,
    specification_of_sum sum1 ->
    alt_specification_of_sum sum2 ->
    forall (h : nat -> nat)
           (c k : nat),
      (sum1 h c) = (sum2 h c).
Proof.
  intros sum1 sum2.
  intros s_sum1 s_sum2.
  intro h.
  intros c k.

  unfold specification_of_sum in s_sum1.
  destruct (s_sum1 h) as [sum1_b sum1_i].
  clear s_sum1.

  unfold alt_specification_of_sum in s_sum2.
  destruct (s_sum2) as [sum2_b sum2_i].
  (* clear s_sum2. *)

  induction c as [ | c' IHc'].
    rewrite -> (sum1_b).
    rewrite -> (sum2_b h).
    reflexivity.

    rewrite -> (sum1_i c').
    rewrite -> (sum2_i h c').
    rewrite -> (IHc').
    reflexivity.
Qed. 

(* ********** *)

(* A first implementation: *)

Fixpoint sum_ds (f : nat -> nat) (n : nat) : nat :=
  match n with
  | 0 => f 0
  | S n' => sum_ds f n' + f n
  end.

Lemma unfold_sum_ds_bc :
  forall f : nat -> nat,
    sum_ds f 0 = f 0.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic sum_ds.
Qed.

Lemma unfold_sum_ds_ic :
  forall (f : nat -> nat)
         (n' : nat),
    sum_ds f (S n') = sum_ds f n' + f (S n').
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic sum_ds.
Qed.

Definition sum_v0 (f : nat -> nat) (n : nat) : nat :=
  sum_ds f n.

Compute unit_tests_for_sum sum_v0.

Theorem sum_v0_satisfies_the_specification_of_sum :
  specification_of_sum sum_v0.
Proof.
  unfold specification_of_sum.
  unfold sum_v0.
  intro f.
  split.
    rewrite -> (unfold_sum_ds_bc f).
    reflexivity.

    intro n.
    rewrite -> (unfold_sum_ds_ic f n).
    reflexivity.
Qed.

(* ********** *)

(* A second implementation: *)

Fixpoint sum_ds' (f : nat -> nat) (n : nat) : nat :=
  match n with
  | 0 => f 0
  | S n' => f n + sum_ds' f n'
  end.

Definition sum_v1 (f : nat -> nat) (n : nat) : nat :=
  sum_ds' f n.

Compute unit_tests_for_sum sum_v1.

Lemma unfold_sum_ds'_bc :
  forall f : nat -> nat,
    sum_v1 f 0 = f 0.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic sum_ds'.
Qed.

Lemma unfold_sum_ds'_ic :
  forall (f : nat -> nat)
         (n' : nat),
    sum_v1 f (S n') = f (S n') + sum_v1 f n'.
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic sum_ds'.
Qed.

Theorem sum_v1_satisfies_the_specification_of_sum :
  specification_of_sum sum_v1.
Proof.
  split.
    rewrite -> (unfold_sum_ds'_bc f).
    reflexivity.

    intro n.
    rewrite -> (unfold_sum_ds'_ic f n).
    rewrite -> (plus_comm (f (S n)) (sum_v1 f n)).
    reflexivity.
Qed.

(*
   Prove the equivalence of sum_v0 and sum_v1.
*)
Theorem sum_v0_and_sum_v1_are_functionally_equal :
  forall (f : nat -> nat)
         (n : nat),
    sum_v0 f n = sum_v1 f n.
Proof.
  intro f.
  intro n.

  unfold sum_v0.

  induction n as [ | n' IHn'].
    rewrite -> (unfold_sum_ds_bc f).
    rewrite -> (unfold_sum_ds'_bc f).
    reflexivity.

    rewrite -> (unfold_sum_ds_ic f).
    rewrite -> (IHn').
    rewrite -> (unfold_sum_ds'_ic f).
    rewrite -> (plus_comm (sum_v1 f n') (f (S n'))).
    reflexivity.
Qed.

(* ********** *)

(* A third implementation: *)

Fixpoint sum_acc (f : nat -> nat) (n a : nat) : nat :=
  match n with
  | 0 => f 0 + a
  | S n' => sum_acc f n' (a + f n)
  end.

Definition sum_v2 (f : nat -> nat) (n : nat) : nat :=
  sum_acc f n 0.

(* Does this implementation fit the specification of sum? *)

Lemma unfold_sum_acc'_bc :
  forall (f : nat -> nat)
         (a : nat),
    sum_acc f 0 a = f 0 + a.
Proof.
  unfold_tactic sum_acc.
Qed.

Lemma unfold_sum_acc_bc :
  forall (f : nat -> nat),
    sum_v2 f 0 = f 0 + 0.
(* left-hand side in the base case
   =
   the corresponding conditional branch *)
Proof.
  unfold_tactic sum_acc.
Qed.

Lemma unfold_sum_acc'_ic :
  forall (f : nat -> nat)
         (n' a : nat),
    sum_acc f (S n') a = sum_acc f n' (a + (f (S n'))).
Proof.
  unfold_tactic sum_acc.
Qed.

Lemma unfold_sum_acc_ic :
  forall (f : nat -> nat)
         (n' : nat),
    sum_v2 f (S n') = sum_v2 f n' + f (S n').
(* left-hand side in the inductive case
   =
   the corresponding conditional branch *)
Proof.
  intro f.
  intro n'.
  revert f.
  induction n' as [ | n'' IHn''].
    intro f.
    rewrite -> (unfold_sum_acc_bc f).
    unfold sum_v2.
    rewrite -> (unfold_sum_acc'_ic f 0).
    rewrite -> (plus_0_l (f 1)).
    rewrite -> (unfold_sum_acc'_bc f).
    rewrite -> (plus_0_r (f 0)).
    reflexivity.

    intro f.
    revert IHn''.
    unfold sum_v2.
    intro IHn''.
    rewrite -> (unfold_sum_acc'_ic f (S n'') 0).
    rewrite -> (IHn'' f).
    rewrite -> (unfold_sum_acc'_ic f n'').
Admitted.
(*
    rewrite <- (unfold_sum_acc'_ic f n'' 0).
    
Qed.
*)
Theorem sum_v2_satisfies_the_specification_of_sum :
  specification_of_sum sum_v2.
Proof.
  split.
    rewrite -> (unfold_sum_acc_bc f).
    rewrite -> (plus_0_r (f 0)).
    reflexivity.

    intro n.
    induction n as [ | n' IHn'].
      rewrite -> (unfold_sum_acc_bc f).
      rewrite -> (unfold_sum_acc_ic f 0).
      rewrite -> (unfold_sum_acc_bc f).
      rewrite -> (plus_0_r (f 0)).
      reflexivity.

      rewrite -> (unfold_sum_acc_ic f (S n')).
      rewrite -> (unfold_sum_acc_ic f n').
      reflexivity.
Qed.

(* ********** *)

(* A fourth implementation: *)

Fixpoint sum_acc' (f : nat -> nat) (n a : nat) : nat :=
  match n with
  | 0 => f 0 + a
  | S n' => sum_acc f n' (f n + a)
  end.

Definition sum_v3 (f : nat -> nat) (n : nat) : nat :=
  sum_acc' f n 0.

(* Does this implementation fit the specification of sum? *)
(* TODO: PROVE THIS *)

(* ********** *)

(* end of week_37a_sum.v *)
