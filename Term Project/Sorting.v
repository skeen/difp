Require Import Arith Bool List.
Require Import unfold_tactic.

Fixpoint ble_nat (n m : nat) : bool :=
  match n with
  | O => true
  | S n' =>
      match m with
      | O => false
      | S m' => ble_nat n' m'
      end
  end.

Lemma ble_nat_bc1 :
  forall m : nat,
    ble_nat 0 m = true.
Proof.
  unfold_tactic ble_nat.
Qed.

Lemma ble_nat_bc2 :
  forall n : nat,
    ble_nat (S n) 0 = false.
Proof.
  unfold_tactic ble_nat.
Qed.

Lemma ble_nat_ic :
  forall n m : nat,
    ble_nat (S n) (S m) = ble_nat n m.
Proof.
  unfold_tactic ble_nat.
Qed.

Definition specification_of_sorted (sorted : list nat -> bool) :=
  (sorted nil = true)
  /\
  (forall n : nat,
     sorted (n :: nil) = true)
  /\
  (forall (n m : nat)
          (nl : list nat),
     sorted (n :: m :: nl) =
     match (ble_nat n m) with
       | true => sorted nl
       | false => false
     end
  ).

Fixpoint sort (nl : list nat) : bool :=
  match nl with
    | nil => true
    | (n :: nil) => true
    | (n :: m :: nl') =>
      match (ble_nat n m) with
        | true => sort nl'
        | false => false
      end
  end.

Lemma sort_bc1 :
    sort nil = true.
Proof.
  unfold_tactic sort.
Qed.

Lemma sort_bc2 :
  forall n : nat,
    sort (n :: nil) = true.
Proof.
  unfold_tactic sort.
Qed.

Lemma sort_ic1 :
  forall (n m : nat)
         (nl : list nat),
    ble_nat n m = false ->
    sort (n :: m :: nl) = false.
Proof.
  intros n m.
  intro nl.
  intro pred.
  unfold sort.
  rewrite -> pred.
  reflexivity.
Qed.

Lemma sort_ic2 :
  forall (n m : nat)
         (nl : list nat),
    ble_nat n m = true ->
    sort (n :: m :: nl) = sort nl.
Proof.
  intros n m.
  intro nl.
  intro pred.
  unfold sort.
  rewrite -> pred.
  reflexivity.
Qed.

Lemma sort_ic_nil :
  forall (n m : nat),
    forall (b : bool),
    ble_nat n m = b ->
    sort (n :: m :: nil) = b.
Proof.
  intros n m b pred.
  unfold sort.
  rewrite -> pred.
  induction b.
    reflexivity.
    reflexivity.
Qed.

Lemma nat_ind2 :
  forall P : nat -> Prop,
    P 0 ->
    P 1 ->
    (forall i : nat,
      P i -> P (S i) -> P (S (S i))) ->
    forall n : nat,
      P n.
Proof.
  intros P H_P0 H_P1 H_PSS n.
  assert (consecutive :
    forall x : nat,
      P x /\ P (S x)).
    intro x.
    induction x as [ | x' [IHx' IHSx']].

    split.
      exact H_P0.
    exact H_P1.

    split.
      exact IHSx'.
    exact (H_PSS x' IHx' IHSx').

  destruct (consecutive n) as [ly _].
  exact ly.
Qed.

Lemma sort_local :
  forall n m : nat,
    sort (S n :: S m :: nil) = sort (n :: m :: nil).
Proof.
Admitted.
(*
  intros n m.
  induction m using nat_ind2.
    rewrite -> (sort_ic2).
    rewrite -> (sort_ic2).
    reflexivity.

    case n.
    rewrite -> ble_nat_bc1.
    reflexivity.

    intro n'.
    rewrite -> ble_nat_bc2.
    reflexivity.

    rewrite -> ble_nat_ic.
    rewrite -> ble_nat_bc1.
    reflexivity.

    rewrite -> (sort_ic_nil _ _ true).
    rewrite -> (sort_ic_nil _ _ true).
    reflexivity.

    case m.
    rewrite -> ble_nat_bc2.
*)
Lemma who_cares :
  forall m : nat,
    ble_nat 0 m = ble_nat 0 (S m).
Proof.
  intro n.
  rewrite ->2 ble_nat_bc1.
  reflexivity.
Qed.

Lemma who_cares2 :
  forall n : nat,
    ble_nat (S n) 0 = ble_nat (S (S n)) 0.
Proof.
  intro n.
  rewrite ->2 ble_nat_bc2.
  reflexivity.
Qed.

Lemma S_is_plus_one :
  forall x : nat,
    S x = 1 + x.
Proof.
  intro x.
  ring.
Qed.

Lemma S_magic :
  forall x y : nat,
    S x + y = S (x + y).
Proof.
Admitted.

Lemma who_cares3 :
  forall n m : nat,
    forall x : nat,
      ble_nat n m = ble_nat n (m + x).
Proof.
  intros n.
  induction n.
    intros m x.
    rewrite ->2 ble_nat_bc1.
    reflexivity.

    intros m x.
    symmetry.
    induction x.
      rewrite -> plus_0_r.
      reflexivity.
      
      rewrite -> (S_is_plus_one x).
      rewrite -> plus_assoc.
      rewrite -> (plus_comm m 1).
      rewrite <- (S_is_plus_one m).
      rewrite <- IHx.
      rewrite -> S_magic.
      rewrite -> ble_nat_ic.
      rewrite <- IHn.
      rewrite -> IHx.
      symmetry.
Abort.
(*
Lemma im_still_ahead_help :
  forall n m : nat,
    forall b : bool,
    ble_nat n (S m) = ble_nat n m.
Proof.
  intros n m b pred.
  induction n.
    rewrite <- pred.
    rewrite ->2 ble_nat_bc1.
    reflexivity.

    intros m b.
    intro pred.
    rewrite <- pred.
    induction m.
    rewrite -> ble_nat_bc2.
    intro pred.
    rewrite <- pred.
    case n.
    rewrite -> ble_nat_bc1.

    intro pred.
    rewrite <- IHn.
    rewrite <- pred.
    induction m.
    rewrite -> ble_nat_ic.
    rewrite <- IHn.

    intro m.
    induction m.
    rewrite -> ble_nat_ic.
    symmetry.
    case n.
    rewrite -> who_cares2.
    rewrite -> IHn.
    Check who_cares.
    rewrite -> 

    rewrite <- ble_nat_ic.

    rewrite <- IHn.
    rewrite -> ble_nat_ic.
    rewrite -> IHn.
*)

Lemma im_still_ahead :
  forall n m : nat,
    forall b : bool,
      ble_nat n m = b ->
      sort (S n :: S m :: nil) = sort (n :: S m :: nil).
Proof.
  intros n m.
  intro b.
  intro pred.
  induction (S m).
    rewrite -> (sort_ic_nil _ _ b).
    rewrite -> (sort_ic_nil _ _ b).
    reflexivity.

    unfold ble_nat.

Lemma sort_satisfy_the_specification_of_sorted :
  specification_of_sorted sort.
Proof.
  unfold specification_of_sorted.
  split.
    exact sort_bc1.
    split.
    exact sort_bc2.
    intro n.
    induction n.
    intros m nl.
    rewrite -> ble_nat_bc1.
    rewrite -> sort_ic2.
    reflexivity.
    
    rewrite -> ble_nat_bc1.
    reflexivity.

    intro m.
    induction m.
    intro nl.
    rewrite -> ble_nat_bc2.
    rewrite -> sort_ic1.
    reflexivity.

    rewrite -> ble_nat_bc2.
    reflexivity.
    
    intro nl.
    rewrite -> ble_nat_ic.
    rewrite <- IHn.

    rewrite -> sort_ic1.


    intro nl.
    rewrite -> ble_nat_ic.
    case n.
      rewrite -> ble_nat_bc1.
      rewrite -> sort_ic_.
    split.
    intros n m.
    intro nl.
Qed.

